<!DOCTYPE html>
<html>
<head>
    <meta name="author" content="Pedro Aguilar Guerrero ITZ-ISC 2018">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
    <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
    <link rel="icon" href="../imagenes/ico.png">

    <title>Crear nuevo Indicador</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href="bootstrap/css/fontawesome-all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
            crossorigin=""></script>
    <!--Pedro-->
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$base_url?>css/calendar.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?=$base_url?>js/es-ES.js"></script>
    <script src="<?=$base_url?>js/jquery.min.js"></script>
    <script src="<?=$base_url?>js/moment.js"></script>
    <!--<script src="<?=$base_url?>js/bootstrap.min.js"></script>-->
    <script src="<?=$base_url?>js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="<?=$base_url?>css/bootstrap-datetimepicker.min.css" />
    <script src="<?=$base_url?>js/bootstrap-datetimepicker.es.js"></script>


    <title>Webslesson Tutorial | Bootstrap Multi Select Dropdown with Checkboxes using Jquery in PHP</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

</head>
<body>
<div class="container">
    <div class="container" style="width:600px;">
        <h2 align="center">Bootstrap Multi Select Dropdown with Checkboxes using Jquery in PHP</h2>
        <br /><br />
        <form method="post" id="framework_form">
            <div class="form-group">
                <label>Select which Framework you have knowledge</label>
                <select id="framework" name="framework[]" multiple class="form-control" >
                    <option style="background-color: white" value="Secretaria de Desarrollo Social">Secretaria de Desarrollo Social</option>
                    <option style="background-color: white" value="Instituto Zacatecano de Cultura">Instituto Zacatecano de Cultura</option>
                    <option style="background-color: white" value="Secretaria de las Mujeres">Secretaria de las Mujeres</option>
                    <option style="background-color: white" value="Sistema Zacatecano de Radio y Televisión">Sistema Zacatecano de Radio y Televisión</option>
                    <option style="background-color: white" value="Consejo Zacatecano de Ciencia y Tecnologia">Consejo Zacatecano de Ciencia y Tecnologia</option>
                    <option style="background-color: white" value="Instituto de la Juventud">Instituto de la Juventud</option>
                    <option style="background-color: white" value="Consejo Zacatecano de Ciencia y Tecnología">Consejo Zacatecano de Ciencia y Tecnología</option>
                    <option style="background-color: white" value="Subsecretaría del Servicio Estatal del Empleo">Subsecretaría del Servicio Estatal del Empleo</option>
                    <option style="background-color: white" value="Instituto de Capacitación para el Trabajo en el Estado de Zacatecas">Instituto de Capacitación para el Trabajo en el Estado de Zacatecas</option>
                    <option style="background-color: white" value="Instituto de Cultura Física y Deporte">Instituto de Cultura Física y Deporte</option>
                    <option style="background-color: white" value="Secretaría De Salud">Secretaría De Salud</option>
                    <option style="background-color: white" value="Centro de Prevención y Atención a la Violencia Familiar y de Género">Centro de Prevención y Atención a la Violencia Familiar y de Género</option>
                    <option style="background-color: white" value="ADELZAC A.C.">ADELZAC A.C.</option>
                    <option style="background-color: white" value="Centro De Integración Juvenil">Centro De Integración Juvenil</option>
                    <option style="background-color: white" value="Secretaría del Campo">Secretaría del Campo</option>
                    <option style="background-color: white" value="Secretaría de Desarrollo Urbano, Ordenamiento Territorial y Vivienda">Secretaría de Desarrollo Urbano, Ordenamiento Territorial y Vivienda</option>
                    <option style="background-color: white" value="Secretaría del Medio Ambiente">Secretaría del Medio Ambiente</option>
                    <option style="background-color: white" value="Secretaría de Obras Públicas">Secretaría de Obras Públicas</option>
                    <option style="background-color: white" value="Apozol">Apozol</option>
                    <option style="background-color: white" value="Apulco">Apulco</option>
                    <option style="background-color: white" value="Atolinga">Atolinga</option>
                    <option style="background-color: white" value="Benito Juárez">Benito Juárez</option>
                    <option style="background-color: white" value="Calera">Calera</option>
                    <option style="background-color: white" value="">Cañitas de Felipe Pescador</option>
                    <option style="background-color: white" value="Chalchihuites">Chalchihuites</option>
                    <option style="background-color: white" value="Concepción del Oro">Concepción del Oro</option>
                    <option style="background-color: white" value="Cuauhtémoc">Cuauhtémoc</option>
                    <option style="background-color: white" value="El Plateado de Joaquín Amaro">El Plateado de Joaquín Amaro</option>
                    <option style="background-color: white" value="El Salvador">El Salvador</option>
                    <option style="background-color: white" value="Fresnillo">Fresnillo</option>
                    <option style="background-color: white" value="Genaro Codina">Genaro Codina</option>
                    <option style="background-color: white" value="General Enrique Estrada">General Enrique Estrada</option>
                    <option style="background-color: white" value="General Francisco R. Murguía">General Francisco R. Murguía</option>
                    <option style="background-color: white" value="General Pánfilo Natera">General Pánfilo Natera</option>
                    <option style="background-color: white" value="Guadalupe">Guadalupe</option>
                    <option style="background-color: white" value="Huanusco">Huanusco</option>
                    <option style="background-color: white" value="Jalpa">Jalpa</option>
                    <option style="background-color: white" value="Jerez">Jerez</option>
                    <option style="background-color: white" value="Jiménez del Teul">Jiménez del Teul</option>
                    <option style="background-color: white" value="Juan Aldama">Juan Aldama</option>
                    <option style="background-color: white" value="Juchipila">Juchipila</option>
                    <option style="background-color: white" value="Loreto">Loreto</option>
                    <option style="background-color: white" value="Luis Moya">Luis Moya</option>
                    <option style="background-color: white" value="Mazapil">Mazapil</option>
                    <option style="background-color: white" value="Melchor Ocampo">Melchor Ocampo</option>
                    <option style="background-color: white" value="Mezquital del Oro">Mezquital del Oro</option>
                    <option style="background-color: white" value="Miguel Auza">Miguel Auza</option>
                    <option style="background-color: white" value="Momax">Momax</option>
                    <option style="background-color: white" value="Monte Escobedo">Monte Escobedo</option>
                    <option style="background-color: white" value="Morelos">Morelos</option>
                    <option style="background-color: white" value="Moyahua de Estrada">Moyahua de Estrada</option>
                    <option style="background-color: white" value="Nochistlán de Mejía">Nochistlán de Mejía</option>
                    <option style="background-color: white" value="Noria de Ángeles">Noria de Ángeles</option>
                    <option style="background-color: white" value="Ojocaliente">Ojocaliente</option>
                    <option style="background-color: white" value="Pánuco">Pánuco</option>
                    <option style="background-color: white" value="Pinos">Pinos</option>
                    <option style="background-color: white" value="Río Grande">Río Grande</option>
                    <option style="background-color: white" value="Sain Alto">Sain Alto</option>
                    <option style="background-color: white" value="Santa María de la Paz">Santa María de la Paz</option>
                    <option style="background-color: white" value="Sombrerete">Sombrerete</option>
                    <option style="background-color: white" value="Susticacán">Susticacán</option>
                    <option style="background-color: white" value="Tabasco">Tabasco</option>
                    <option style="background-color: white" value="Tepechitlán">Tepechitlán</option>
                    <option style="background-color: white" value="Tepetongo">Tepetongo</option>
                    <option style="background-color: white" value="Teúl de González Ortega">Teúl de González Ortega</option>
                    <option style="background-color: white" value="Tlaltenango de Sánchez Román">Tlaltenango de Sánchez Román</option>
                    <option style="background-color: white" value="Trancoso">Trancoso</option>
                    <option style="background-color: white" value="Trinidad García de la Cadena">Trinidad García de la Cadena</option>
                    <option style="background-color: white" value="Valparaíso">Valparaíso</option>
                    <option style="background-color: white" value="Vetagrande">Vetagrande</option>
                    <option style="background-color: white" value="Villa de Cos">Villa de Cos</option>
                    <option style="background-color: white" value="Villa García">Villa García</option>
                    <option style="background-color: white" value="Villa González Ortega">Villa González Ortega</option>
                    <option style="background-color: white" value="Villa Hidalgo">Villa Hidalgo</option>
                    <option style="background-color: white" value="Villanueva">Villanueva</option>
                    <option style="background-color: white" value="Zacatecas">Zacatecas</option>
                    <option style="background-color: white" value="Secretaría de Educación">Secretaría de Educación</option>
                    <option style="background-color: white" value="Servicio de Estatal para el Desarrollo Integral de la Familia">Servicio de Estatal para el Desarrollo Integral de la Familia</option>
                    <option style="background-color: white" value="Comisión de Derechos Humanos">Comisión de Derechos Humanos</option>
                    <option style="background-color: white" value="Secretaría de Seguridad Pública">Secretaría de Seguridad Pública</option>
                    <option style="background-color: white" value="Secretaría General de Gobierno">Secretaría General de Gobierno</option>
                    <option style="background-color: white" value="Subsecretaría del Transporte Público">Subsecretaría del Transporte Público</option>
                    <option style="background-color: white" value="Fiscalia General de Justicia del Estado de Zacatecas">Fiscalia General de Justicia del Estado de Zacatecas</option>
                    <option style="background-color: white" value="Consejo Estatal Ciudadano">Consejo Estatal Ciudadano</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit" />
            </div>
        </form>
        <br />
    </div>
    <script>
        $(document).ready(function(){
            $('#framework').multiselect({
                nonSelectedText: 'Select Framework',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonWidth:'400px'
            });

            $('#framework_form').on('submit', function(event){
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax({
                    url:"insert.php",
                    method:"POST",
                    data:form_data,
                    success:function(data)
                    {
                        $('#framework option:selected').each(function(){
                            $(this).prop('selected', false);
                        });
                        $('#framework').multiselect('refresh');
                        alert(data);
                    }
                });
            });


        });
    </script>
</div>
</body>
</html>