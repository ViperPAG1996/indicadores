<?php
error_reporting(0);

// Incluimos nuestro archivo config
include '../conexion/config.php';
include  'funciones.php';
// Sentencia sql para traer los eventos desde la base de datos
$sql="SELECT * FROM tbl_Actividades";
// Verificamos si existe un dato
if ($conexion->query($sql)->num_rows)
{
    // creamos un array
    $datos = array();
    $dias_arreglo = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    //guardamos en un array multidimensional todos los datos de la consulta
    $i=0;
    // Ejecutamos nuestra sentencia sql
    $e = $conexion->query($sql);
    while($row=$e->fetch_array()) // realizamos un ciclo while para traer los eventos encontrados en la base de dato
    {
        // Alimentamos el array con los datos de los eventos
        if(substr($row[15],0,10) == substr($row[16],0,10)) {
            array_push($datos, $row);
        }elseif ($row[20]=="Infraestructura"){
            array_push($datos, $row);
        }else{
            $dependencia = $row[dias];
            $dependencia = $dependencia . ' ';
            //var_dump($row[dias]);
            $arrdep = array();

            do {
                $dependencia1 = strstr($dependencia, ' ', true);
                $dependencia2 = strstr($dependencia, ' ');
                $dependencia = substr($dependencia2, 1);
                //var_dump($dependencia);
                if($dependencia1 == 'lunes'){
                    array_push($arrdep, 'Monday');
                }
                if($dependencia1 == 'martes'){
                   array_push( $arrdep, 'Tuesday');
                }
                if($dependencia1 == 'miercoles'){
                   array_push( $arrdep, 'Wednesday');
                }
                if($dependencia1 == 'jueves'){
                    array_push($arrdep ,'Thursday');
                }
                if($dependencia1 == 'viernes'){
                   array_push( $arrdep, 'Friday');
                }
                if($dependencia1 == 'sabado'){
                    array_push($arrdep, 'Saturday');
                }
                if($dependencia1 == 'domingo'){
                    array_push($arrdep, 'Sunday');
                }
            } while ($dependencia != '');
            //var_dump($arrdep);
            $year = substr($row[15], 6, 4);
            $mes = substr($row[15], 3, 2);
            $day = substr($row[15], 0, 2);

            $year1 = substr($row[16], 6, 4);
            $mes1 = substr($row[16], 3, 2);
            $day1 = substr($row[16], 0, 2);

            $fecha1 = new DateTime($year . "-" . $mes . "-" . $day);
            $fecha2 = new DateTime($year1 . "-" . $mes1 . "-" . $day1);
            $resultado = $fecha1->diff($fecha2);
            $dias_res = $resultado->format('%a');

            $dia_in = $year."-".$mes."-".$day;
            $dia_fn = $year1 . "-" . $mes1 . "-" . $day1;
            for ($i =0; $i<=$dias_res; $i++){
                $a = array();
                $fechaFFase=$dia_in;
                $nuevafecha = new DateTime($fechaFFase);
                $nuevafecha2 = new DateTime($fechaFFase);
                $nuevafecha2->modify('+1 day');
                $dia_in2 = $nuevafecha2->format('d-m-Y');
                //var_dump(strftime('%A', strtotime($dia_in)));
                //var_dump($arrdep);
                if(in_array(strftime('%A', strtotime($dia_in)), $arrdep)){
                    //var_dump("Existe");
                    $a[0] = $row[0];
                    $a[id] = $row[0];
                    $a[1] = $row[1];
                    $a[title] = $row[1];
                    $a[2] = $row[2];
                    $a[factor] = $row[2];
                    $a[3] = $row[3];
                    $a[tipo] = $row[3];
                    $a[4] = $row[4];
                    $a[dependencia] = $row[4];
                    $a[5] = $row[5];
                    $a[lugar] = $row[5];
                    $a[6] = $row[6];
                    $a[lon] = $row[6];
                    $a[7] = $row[7];
                    $a[lat] = $row[7];
                    $a[8] = $row[8];
                    $a[monto] = $row[8];
                    $a[9] = $row[9];
                    $a[personas] = $row[9];
                    $a[10] = $row[10];
                    $a[body] = $row[10];
                    $a[11] = $row[11];
                    $a[url] = $row[11];
                    $a[12] = $row[12];
                    $a[classe] = $row[12];
                    $a[13] = _formatear($dia_in2);
                    $a[start] = _formatear($dia_in2);
                    $a[14] = _formatear($dia_in2);
                    $a[end] = _formatear($dia_in2);
                    $a[15] = $row[15];
                    $a[inicio_normal] = $row[15];
                    $a[16] = $row[16];
                    $a[final_normal] = $row[16];
                    $a[17] = $row[17];
                    $a[dias] = $row[17];
                    $a[18] = $row[18];
                    $a[fecha_creacion] = $row[18];
                    $a[19] = $row[19];
                    $a[creador] = $row[19];
                    $a[20] = $row[20];
                    $a[actividad] = $row[20];
                    $a[21] = $row[21];
                    $a[archivo] = $row[21];
                    //print_r($a);
                    array_push($datos, $a);

                }
                $nuevafecha->modify('+1 day');
                $dia_in = $nuevafecha->format('Y-m-d');


            }
        }
    }
    // Transformamos los datos encontrado en la BD al formato JSON
    echo json_encode(
        array(
            "success" => 1,
            "result" => $datos
        )
    );
}
else
{
    // Si no existen eventos mostramos este mensaje.
    echo "No hay datos";
}
?>

