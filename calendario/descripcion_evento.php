<?php
    //incluimos nuestro archivo config
    include '../conexion/config.php';
    // Incluimos nuestro archivo de funciones
    include '../calendario/funciones.php';
    // Obtenemos el id del evento
    $id  = evaluar($_GET['id']);
    // y lo buscamos en la base de dato
    $bd  = $conexion->query("SELECT * FROM tbl_actividades WHERE id=$id");
    // Obtenemos los datos
    $row = $bd->fetch_assoc();
    // titulo 
    $titulo=$row['title'];
    // factor
    $factor=$row['factor'];
    // tipo
    $tipo=$row['tipo'];
    // dependencias
    $dependencia=$row['dependencia'];
    // lugar
    $lugar=$row['lugar'];
    // cuerpo
    $evento=$row['body'];
    // Fecha inicio
    $inicio=$row['inicio_normal'];
    // Fecha Termino
    $final=$row['final_normal'];
    // estado
    $class = $row['class'];
// Eliminar evento
if (isset($_POST['eliminar_evento'])) {
    $id  = evaluar($_GET['id']);
    $sql = "DELETE FROM tbl_actividades WHERE id = $id";
    if ($conexion->query($sql)) 
    {
        echo "Evento eliminado";
        echo '<script> window.location="calendario.php"; </script>';
    }
    else
    {
        echo "El evento no se pudo eliminar";
    }
}
 ?>

<!DOCTYPE html>
<html lang="en">
    <head>
	   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
    <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
    <link rel="icon" href="../imagenes/ico.png">
	   <title><?=$titulo?></title>
           <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script src="../bootstrap/js/jquery.min.js"></script>
    <script src="../bootstrap/js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
        crossorigin=""></script>
    </head>
    <body>
        <h3><?=$titulo?></h3>
        <hr>
        <b>Fecha inicio:</b> <?=$inicio?>
        <b>Fecha termino:</b> <?=$final?><br>
        <b>Factor:</b> <?=$factor?><br>
        <b>Tipo de actividad:</b> <?=$tipo?><br>
        <b>Dependencia/s:</b> <?=$dependencia?><br>
        <b>Lugar:</b> <?=$lugar?><br>
        <b>Descripcion:</b><br><?=$evento?><br>
        <b>Estado del evento:</b>
        <?php
        if($class == 'event-info') {
            echo '<p class="bg-info text-white">Terminado.</p>';
        } if ($class == 'event-success') {
            echo '<p class="bg-success text-white">Ultimos preparativos.</p>';
        } if ($class == 'event-warning') {
            echo '<p class="bg-warning text-white">En Proceso.</p>';
        } if ($class == 'event-important') {
            echo '<p class="bg-danger text-white">Necesita ser atendido.</p>';
        } if ($class == 'event-dark') {
            echo '<p class="bg-dark text-white">Abandonado necesita ser atendido.</p>';
        }
        ?>
    </body>
    <form action="" method="post">
<!--            <a href="../actividades/readform.php?read_id=--><?php //echo $row['id']; ?><!--" role="button" title="Ver"-->
<!--               class="btn btn-outline-warning"> <i class="fas fa-eye"></i> </a>-->
<!--            <a href="../actividades/editform.php?edit_id=--><?php //echo $row['id']; ?><!--" role="button"-->
<!--               title="Editar datos" class="btn btn-outline-info"> <i class="fas fa-pencil-alt"></i> </a>-->
<!--            <a name="delete_id" href="?delete_id=--><?php //echo $row['id']; ?><!--" role="button" title="Eliminar"-->
<!--               class="btn btn-outline-danger"> <i class="far fa-trash-alt"></i> </a>-->
            <button type="submit" class="btn btn-danger" name="eliminar_evento">Eliminar</button>
    </form>
</html>
