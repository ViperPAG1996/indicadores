<?php
	error_reporting( ~E_NOTICE );	
	require_once '../conexion/dbconfig.php';
	
	if(isset($_GET['read_id']) && !empty($_GET['read_id']))
	{
		$id = $_GET['read_id'];
		$stmt_edit = $DB_con->prepare('SELECT * FROM eventos WHERE id =:id');
        //userName, userProfession, userPic FROM tbl_users WHERE userID =:uid');
		$stmt_edit->execute(array(':id'=>$id));
		$edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
		extract($edit_row);
	}
	else
	{
		header("Location: index.php");
	}	
	
?>
<!DOCTYPE html>
<html lang="es">

<head>
        <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
    <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
    <link rel="icon" href="../imagenes/ico.png">

    <title>Indicador $accion</title>

    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script src="../bootstrap/js/jquery.min.js"></script>
    <script src="../bootstrap/js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
        crossorigin=""></script>

    <style>      
    .inputstylo:focus {
     background-color: #E9ECEF;
     border: none;
     border-radius: 15px;
     outline: none ;
     box-shadow: none;
   }
              .inputstylo {
     background-color: #E9ECEF;
     border: none;
     border-radius: 15px;
   } 
      </style>
</head>

<body style="background-color: #E9ECEF">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="navbar-toggler-icon"></span>
                    </button> <a class="navbar-brand" href="#">Subsecretaría </a>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="navbar-nav ml-md-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                                    data-toggle="dropdown">Nombre Usuario</a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="calendario/calendario.php">Ver Calendario</a>
                                    <a class="dropdown-item" href="graficas/graficas.php">Ver Graficas</a>
                                    <a class="dropdown-item" href="usuarios/usermod.php">Manejo de usuarios</a>
                                    <div class="dropdown-divider">
                                    </div>
                                    <a class="dropdown-item" href="../logout.php">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="row">
                    <main role="main" class="container">
                        <div class="page-header p-3">
                            <br>
                            <br>
                            <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                <h6 class="mb-0 text-white lh-100">Modificar</h6>
                            </div>
                          </div>
                        <div class="p-3 bg-white rounded shadow-sm">
                                <table class="table table-borderless table-sm">
                                        <tbody>
                                  
                                          <tr >
                                          <td>
                                                <div class="form-group border  p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                    <span><b>Titulo (Linea Base Indicador):</b></span>
                                                
                                                  <?php echo $accion; ?>
                                                
                                                    </div>
                                                    
                                            </td>
                                            <td width="50%" rowspan="9">
                                                <center>
                                            <div id='map' style="height:40vw; width: 100%;"></div>
                                        </center>
                                        
                                        <script>
        
        var map = L.map('map').setView([<?php echo $longuitud; ?>], 20);
            
    
    // Capas base
    var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap<\/a> contributors'
       
    }).addTo(map);
            
            var punto = L.marker([<?php echo $longuitud; ?>]);
    punto.addTo(map);
    punto.bindPopup('Evento').openPopup();
        </script>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td >
                                                    
                                                    <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                                <span><b>Factor de Riesgo:</b></span>
                                                                <?php echo $factor; ?>
                                                                </div>
                                            </td>		
                                          </tr>    
                                          <tr>
                                                <td >
                                                        
                                                        <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                                    <span><b>Tipo de Actividad:</b></span>
                                                                    <?php
                                                    if ($class == 'event-success'){
                                                        echo '<p class="bg-success text-white">Recreativa.</p>';
                                                    } elseif ($class == 'event-warning'){
                                                        echo '<p class="bg-warning text-white">Delictiva Nivel Bajo.</p>';
                                                        } elseif ($class == 'event-danger'){
                                                        echo '<p class="bg-danger text-white">Actividad Delictiva Nivel Alto.</p>';
                                                } else {
                                                        echo '<p class="bg-secondary text-white">Error.</p>';
                                                    }
                                                    ?>     
                                                                </div>
                                                </td>		
                                              </tr> 
                                          <tr >
                                            <td> 
                                                    <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                    <span><b>Dependencias Involucradas:</b></span>                                                    <p class="p-1" >
                                                    <?php echo $dependencias; ?>
                                                        </p>    
                                                </div>
                                            </td>
                                          </tr>
                                         
                                          <tr >
                                              <!-- // <?php echo date("h:i a", strftime($final_normal));?> -->
                                              <td>
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Fecha:</b></span><br>
                                                <input type="date" name="fromd" for="fromd" id="fromd" value="29/03/2018" class="form-control inputstylo">
                                                <span><b>Hora Inicial:</b></span>
                                                <input type="time" name="fromc" id="fromc" value="16:32" class="form-control inputstylo">
                                                <span><b>Hora Final:</b></span>
                                                <input type="time" name="toc" id="toc" value="16:32" class="form-control inputstylo">
                                            </div>
                                        </td>
                                          </tr>
                                          <tr >
                                            <td>
                                                    <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                    <span><b>Estado de actividad:</b></span>
                                                
                                                    <?php
                                                    if ($class == 'event-info'){
                                                        echo '<p class="bg-info text-white">Terminado.</p>';
                                                    }elseif ($class == 'event-success'){
                                                        echo '<p class="bg-success text-white">Ultimos preparativos.</p>';
                                                    } elseif ($class == 'event-warning'){
                                                        echo '<p class="bg-warning text-white">En Proceso.</p>';
                                                        } elseif ($class == 'event-important'){
                                                        echo '<p class="bg-danger text-white">Desatendido.</p>';
                                                }
                                                elseif ($class == 'event-dark'){
                                                    echo '<p class="bg-dark text-white">Abandonado.</p>';
                                                } else {
                                                        echo '<p class="bg-light text-white">Error.</p>';
                                                    }
                                                    ?>     
                                                </div>
                                            </td>
                                          </tr>
                                          <tr >
                                            <td>
                                                    <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                    <span><b>Descripcion del evento:</b></span>
                                                    <p class="p-1" >
                                                    <?php echo $body; ?>
                                                        </p>    
                                                </div>
                                            </td>
                                          </tr>
                                          <tr >
                                                <td colspan="2">
                                         
                                                </td>
                                            </tr>    
        
                                            </tbody>
                                                </table>
                        </div>
                        <br>
                        <center>
                        <div class="btn-group btn-group-lg">
                         <a class="btn btn-warning " href="editform.php?update_id=<?php echo $row['id']; ?>" role="button"><i class="fas fa-times"></i> Modificar </a> 
                         <a class="btn btn-secondary " href="../view.php" role="button"><i class="fas fa-times"></i> Imprimir </a>
                         <a class="btn btn-danger " href="../view.php" role="button"><i class="fas fa-times"></i> Regresar </a>
                                                </div>
                                                </center>
                         <br><br>
                         
                      </main>
                  
                    </div>
                </div>
            </div>
           
        </div>
    </div>

</body>

</html>