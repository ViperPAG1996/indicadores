<?php
session_start();
date_default_timezone_set("America/Mexico_City");
include '../calendario/funciones.php';
include '../conexion/config.php';
if (isset($_POST['fromd'])) {
    if ($_POST['fromd'] != "") {
        $actividad = $_POST['actividad'];
        if ($actividad == "Actividad"){
        //Fecha y hora inicial
        //Fecha
        $iniciod = date_create($_POST['fromd']);
        //Hora
        $inicioc = $_POST['fromc'];
        //Fecha y hora del calendario
        $newfechai = date_format($iniciod, "d/m/Y");
        $iniciot = $newfechai . " " . $inicioc;
        $inicio = _formatear($iniciot);
        $inicio_normal = $iniciot;
        //Fecha y hora final
        $finald = $iniciod;
        $finalc = $_POST['toc'];
        //Fecha y hora del calendario
        $newfechaf = date_format($finald, "d/m/Y");
        $finalt = $newfechaf . " " . $finalc;
        $final = _formatear($finalt);
        $final_normal = $finalt;
        //Dias
        $dias = $_POST['dia_sem'];
    }elseif ($actividad == "Repetitiva") {
        //Fecha y hora inicial
        //Fecha
        $iniciod = date_create($_POST['fromd']);
        //Hora
        $inicioc = $_POST['fromc'];
        $newfechai = date_format($iniciod, "d/m/Y");
        $iniciot = $newfechai . " " . $inicioc;
        $inicio = _formatear($iniciot);
        $inicio_normal = $iniciot;
        //Fecha y hora final
        //Fecha
        $finald = date_create($_POST['tod']);
        //Hora
        $finalc = $_POST['toc'];
        $newfechaf = date_format($finald, "d/m/Y");
        $finalt = $newfechaf . " " . $finalc;
        $final = _formatear($finalt);
        $final_normal = $finalt;
        //Dias
        $dias = "";
        if (isset($_REQUEST['domingo'])) {
            $d = 1;
            $dias = $dias . " domingo";
        } else {
            $d = 0;
        }
        if (isset($_REQUEST['lunes'])) {
            $l = 1;
            $dias = $dias . " lunes";
        } else {
            $l = 0;
        }
        if (isset($_REQUEST['martes'])) {
            $m = 1;
            $dias = $dias . " martes";
        } else {
            $m = 0;
        }
        if (isset($_REQUEST['miercoles'])) {
            $mi = 1;
            $dias = $dias . " miercoles";
        } else {
            $mi = 0;
        }
        if (isset($_REQUEST['jueves'])) {
            $j = 1;
            $dias = $dias . " jueves";
        } else {
            $j = 0;
        }
        if (isset($_REQUEST['viernes'])) {
            $v = 1;
            $dias = $dias . " viernes";
        } else {
            $v = 0;
        }
        if (isset($_REQUEST['sabado'])) {
            $s = 1;
            $dias = $dias . " sabado";
        } else {
            $s = 0;
        }
    }elseif ($actividad == "Infraestructura") {
        //Fecha y hora inicial
        //Fecha
        $iniciod = date_create($_POST['fromd']);
        //Hora
        $inicioc = $_POST['fromc'];
        $newfechai = date_format($iniciod, "d/m/Y");
        $iniciot = $newfechai . " " . $inicioc;
        $inicio = _formatear($iniciot);
        $inicio_normal = $iniciot;
        //Fecha y hora final
        //Fecha
        $finald = date_create($_POST['tod']);
        //Hora
        $finalc = $_POST['toc'];
        $newfechaf = date_format($finald, "d/m/Y");
        $finalt = $newfechaf . " " . $finalc;
        $final = _formatear($finalt);
        $final_normal = $finalt;
        //Dias
        $dias = "";
    }
        //Titulo
        $titulo = evaluar($_POST['title']);
        //Descripcion
        $body = evaluar($_POST['event']);
        //Estado
        $clase = evaluar($_POST['class']);
        //Factor
        if (isset($_POST["factor"])) {
            $factor = '';
            foreach ($_POST["factor"] as $row) {
                $factor .= $row . ', ';
            }
            $factor = substr($factor, 0, -2);
        }
        //Tipo de actividad
        if (isset($_POST["tipo"])) {
            $tipo = '';
            foreach ($_POST["tipo"] as $row) {
                $tipo .= $row . ', ';
            }
            $tipo = substr($tipo, 0, -2);
        }
        //Dependencias
        if (isset($_POST["framework"])) {
            $framework = '';
            foreach ($_POST["framework"] as $row) {
                $framework .= $row . ', ';
            }
            $framework = substr($framework, 0, -2);
        }
        //Coordenadas del mapa
        $lon = $_POST['lon'];
        $lat = $_POST['lat'];
        //Lugar
        $lugar = $_POST['lugar'];
        //Creador del evento
        $creador = $_SESSION['user'];
        //Monto
        $monto = $_POST['monto'];
        if ($_POST['monto'] == "") {
            $monto = 0;
        }
        //Personas beneficiadas
        $personas = $_POST['personas'];
        if ($_POST['personas'] == "") {
            $personas = " ";
        }
        //Archivos
        $imgFile = $_FILES['archi']['name'];
        $tmp_dir = $_FILES['archi']['tmp_name'];
        $imgSize = $_FILES['archi']['size'];
        $upload_dir = 'archivos/'; // upload directory
        $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); // get image extension
        // vad image extensions
        $vad_extensions = array('jpeg', 'jpg', 'png', 'pdf', 'doc', 'docx', 'xlsx', 'xls'); // vad extensions
        // rename uploading image
        $archivo = rand(100, 1000000) . "." . $imgExt;
        // allow vad image file formats
        if (in_array($imgExt, $vad_extensions)) {
            // Check file size '2MB'
            if ($imgSize < 2000000) {
                move_uploaded_file($tmp_dir, $upload_dir . $archivo);
            } else {
                $errMSG = "El archivo es muy pesado, el mite es de 2 Mb.";
            }
        } else {
            $errMSG = "La extencion del archivo no se acepta, solo extenciones: jpg, jpeg, png, pdf, doc y docx.";
        }
        if (!$conexion) {
            die("No se puede usar la base de datos" . mysqli_error($con));
        } else {
            // Ejecutamos nuestra sentencia sql
            $query = "INSERT INTO tbl_actividades (title, factor, tipo, dependencia, lugar, lon, lat, monto, personas, body, url, class, start, end , inicio_normal, final_normal, dias, creador, actividad, archivo ) VALUES ('$titulo','" . $factor . "', '" . $tipo . "','" . $framework . "', '$lugar', $lon, $lat, $monto, '$personas', '$body', '', '$clase', '$inicio', '$final', '$inicio_normal', '$final_normal', '$dias', '$creador', '$actividad', '$archivo')";
        }
        echo $query;
        $conexion->query($query);
        // Obtenemos el ultimo id insetado
        $im = $conexion->query("SELECT MAX(id) AS id FROM tbl_actividades");
        $row = $im->fetch_row();
        $id = trim($row[0]);
        // para generar el link del evento
        $link = "$base_url" . "calendario/descripcion_evento.php?id=$id";
        // y actualizamos su link
        $query = "UPDATE tbl_actividades SET url = '$link' WHERE id = $id";
        // Ejecutamos nuestra sentencia sql
        $conexion->query($query);
        // redireccionamos a nuestro calendario/
        echo '<script> window.location="../view.php"; </script>';
    }
}
?>
