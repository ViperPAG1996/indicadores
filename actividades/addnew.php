<?php
session_start();
include '../conexion/config.php';
include '../conexion/dbconfig.php';
if (isset($_GET['act']) && !empty($_GET['act'])) {
    $act = $_GET['act'];
} else {
    header("Location: ../view.php");
}
if (isset($_SESSION['user'])) { ?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta name="author" content="Pedro Aguilar Guerrero ITZ-ISC 2018">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta name="description"
              content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
        <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
        <link rel="icon" href="../imagenes/ico.png">
        <title>Nueva actividad</title>

        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <script src="../bootstrap/js/jquery.min.js"></script>
        <script src="../bootstrap/js/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
              integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
              crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
                integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
                crossorigin=""></script>
        <script type="text/javascript">
            function validar() {
                var txtAccion = document.getElementById('title').value;
                var txtFactor = document.getElementById('factor').value;
                var txtDependencias = document.getElementById('dependencia').value;
                var txtFecha = document.getElementById('fecha_inicio').value;
                if (txtAccion == null || txtAccion.length == 0 || /^\s+$/.test(txtAccion)) {
                    alert('ERROR: El campo accion no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if (txtFactor == null || txtFactor.length == 0 || /^\s+$/.test(txtFactor)) {
                    alert('ERROR: El campo factor no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if (txtDependencias == null || txtDependencias.length == 0 || /^\s+$/.test(txtDependencias)) {
                    alert('ERROR: El campo dep no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if (!isNaN(txtFecha)) {
                    alert('ERROR: Debe elegir una fecha');
                    return false;
                }
                return true;
            }
        </script>
        <style>
            .inputstylo:focus {
                background-color: #E9ECEF;
                border: none;
                border-radius: 15px;
                outline: none;
                box-shadow: none;
            }

            .inputstylo {
                background-color: #E9ECEF;
                border: none;
                border-radius: 15px;
            }

            .dropdown-menu {
                max-height: 350px;
                overflow-y: auto;
                overflow-x: hidden;
            }

            $
            (
            '#sandbox-container .input-group.date'
            )
            .

            datepicker
            (
            {
                daysOfWeekHighlighted: "1,2,3,4,5",
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true
            }
            )
            ;

        </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css"/>

        <script src="../dist/js/BsMultiSelect.js"></script>
        <link rel="stylesheet" href="../dist/css/BsMultiSelect.css">
    </head>

    <body style="background-color: #E9ECEF">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="#">Subsecretaría</a>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="navbar-nav ml-md-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com"
                                   id="navbarDropdownMenuLink"
                                   data-toggle="dropdown"><?php echo $_SESSION['user']; ?></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="calendario/calendario.php">Ver Calendario</a>
                                    <a class="dropdown-item" href="graficas/graficas.php">Ver Graficas</a>
                                    <a class="dropdown-item" href="usuarios/usermod.php">Manejo de usuarios</a>
                                    <div class="dropdown-divider">
                                    </div>
                                    <a class="dropdown-item" href="logout.php">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <div class="page-header p-3">
                            <br>
                            <br>
                            <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                <h6 class="mb-0 text-white lh-100">Agregar Nueva Actividad<i class="fas fa-circle"></i>
                                </h6>
                            </div>
                        </div>
                        <div class="jumbotron" style="background-color: white">
                            <form action="cbd.php" method="POST" enctype="multipart/form-data" class="form-horizontal" onsubmit="return validar()">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Titulo del evento:</b></span>
                                                <input type="text" class="form-control inputstylo" name="title"
                                                       id="title" for="title" placeholder="Introduce un Titulo"
                                                       class="form-control inputstylo">
                                            </div>

                                        </td>
                                        <td width="50%" rowspan="11">
                                            <center>
                                                <input id="lon" name="lon" type="hidden" >
                                                <input id="lat" name="lat" type="hidden">
                                                <div class="input" id="buscador">
                                                    <input type="text" class="form-control inputstylo" id="lugar" name="lugar" placeholder="Ingrese Direccion y/o busque en el mapa"  >
                                                    <button class="btn btn-outline-secondary btn-block" type="button" onclick="direccion_buscador();">Buscar</button>
                                                    <div id="resultado"/>
                                                </div>
                                                <br>
                                                <div id='map' style="height:40vw; width: 100%;"></div>

                                            </center>

                                            <script>
                                                var map = L.map('map').setView([22.775484099952596, -102.57200717926025], 10);
                                                // Capas base
                                                var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                                                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap<\/a> contributors'

                                                }).addTo(map);
                                                var feature;
                                                function elegirDireccion(lat1, lng1, lat2, lng2, tipo_osm) {
                                                    var loc1 = new L.LatLng(lat1, lng1);
                                                    var loc2 = new L.LatLng(lat2, lng2);
                                                    var bounds = new L.LatLngBounds(loc1, loc2);

                                                    if (feature) {
                                                        map.removeLayer(feature);
                                                    }
                                                    if (tipo_osm == "node") {
                                                        feature = L.circle( loc1, 25, {color: 'green', fill: false}).addTo(map);
                                                        map.fitBounds(bounds);
                                                        map.setZoom(18);
                                                    }else{
                                                        var loc3 = new L.LatLng(lat1, lng2);
                                                        var loc4 = new L.LatLng(lat2, lng1);

                                                        //  feature = L.polyline( [loc1, loc4, loc2, loc3, loc1], {color: 'red'}).addTo(map);
                                                        map.fitBounds(bounds);
                                                    }
                                                }

                                                function direccion_buscador() {
                                                    var entrada = document.getElementById("lugar");

                                                    $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=5&q=' + entrada.value, function(data) {
                                                        var items = [];

                                                        $.each(data, function(key, val) {
                                                            bb = val.boundingbox;
                                                            items.push("<li><a href='#' onclick='elegirDireccion(" + bb[0] + ", " + bb[2] + ", " + bb[1] + ", " + bb[3] + ", \"" + val.tipo_osm + "\");return false;'>" + val.display_name + '</a></li>');
                                                        });

                                                        $('#resultado').empty();
                                                        if (items.length != 0) {
                                                            $('<p>', { html: "Resultados de la b&uacute;queda:" }).appendTo('#resultado');
                                                            $('<ul/>', {
                                                                'class': 'my-new-list',
                                                                html: items.join('')
                                                            }).appendTo('#resultado');
                                                        }else{
                                                            $('<p>', { html: "Ningun resultado encontrado." }).appendTo('#resultado');
                                                        }
                                                    });
                                                }
                                                var popup = L.popup();
                                                function simpleReverseGeocoding(lon, lat) {
                                                    fetch('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat).then(function(response) {
                                                        return response.json();
                                                    }).then(function(json) {

                                                        var lugar = document.getElementById('lugar').value = json.display_name.toString();
                                                    })
                                                }
                                                function onMapClick(e) {
                                                    var lon = document.getElementById('lon').value = e.latlng.lng.toString();
                                                    var lat = document.getElementById('lat').value = e.latlng.lat.toString();
                                                    simpleReverseGeocoding(lon, lat);
                                                    popup
                                                        .setLatLng(e.latlng) // Sets the geographical point where the popup will open.
                                                        .setContent('<center><i class="far fa-dot-circle fa-2x"></i></center>') // Sets the HTML content of the popup.
                                                        .openOn(map); // Adds the popup to the map and closes the previous one.
                                                }
                                                map.on('click', onMapClick);
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Factor de Riesgo:</b></span>
                                                <select name="factor[]" id="factor" class="custom-select inputstylo"
                                                        multiple="multiple">
                                                    <option disabled>Seleccione el factor de riesgo:</option>
                                                    <option style="background-color: white"
                                                            value="Exclusión Social Del Entorno Urbano">Exclusión Social
                                                        Del Entorno Urbano
                                                    </option>
                                                    <option style="background-color: white" value="Pobreza Cultural">
                                                        Pobreza Cultural
                                                    </option>
                                                    <option style="background-color: white" value="Deserción Escolar">
                                                        Deserción Escolar
                                                    </option>
                                                    <option style="background-color: white" value="Embarazos Juveniles">
                                                        Embarazos Juveniles
                                                    </option>
                                                    <option style="background-color: white"
                                                            value="Precarización Del Empleo">Precarización Del Empleo
                                                    </option>
                                                    <option style="background-color: white"
                                                            value="Poca Confianza En Las Instituciones">Poca Confianza
                                                        En Las Instituciones
                                                    </option>
                                                    <option style="background-color: white" value="Adicciones">
                                                        Adicciones
                                                    </option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Tipo de Acción:</b></span>
                                                <select name="tipo[]" id="tipo" class="custom-select inputstylo"
                                                        multiple="multiple">
                                                    <option disabled>Seleccione tipo de acción:</option>
                                                    <option style="background-color: blue" value="Cultura">Cultura
                                                    </option>
                                                    <option style="background-color: green" value="Empleo">Empleo
                                                    </option>
                                                    <option style="background-color: yellow" value="Deportes">Deportes
                                                    </option>
                                                    <option style="background-color: orange" value="Adicciones">
                                                        Adicciones
                                                    </option>
                                                    <option style="background-color: red" value="Urbanismo(Obras)">
                                                        Urbanismo(Obras)
                                                    </option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <form method="post" id="framework_form">
                                                    <div class="form-group">
                                                        <span><b>Dependencias Involucradas:</b></span>
                                                        <select name="framework[]" id="framework"
                                                                class="form-control inputstylo" multiple="multiple">
                                                            <?php
                                                            $stmt = $DB_con->prepare('SELECT * FROM tbl_dep');
                                                            $stmt->execute();
                                                            if ($stmt->rowCount() > 0) {
                                                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                                    extract($row);
                                                                    ?>
                                                                    <option id="<?php echo $row['id'] ?>"
                                                                            name="<?php echo $row['user'] ?>"
                                                                            value="<?php echo $row['user'] ?>"><?php echo $row['user'] ?></option>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <div class="col-xs-12">
                                                                    <div class="alert alert-warning">
                                                                        <span class="glyphicon glyphicon-info-sign"></span>
                                                                        &nbsp; Datos no encontrados ...
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Personas beneficiadas (opcional):</b></span>
                                                <input type="text" class="form-control inputstylo" name="personas"
                                                       id="personas" for="personas"
                                                       placeholder="Ingrese a las personas a las que ayuda el evento"
                                                       class="form-control inputstylo">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Monto de inversion (opcional):</b></span>
                                                <input type="number" class="form-control inputstylo" name="monto"
                                                       id="monto" placeholder="Ingrese la cantidad de la inversion">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Informacion del calendario:</b></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <?php
                                                if ($act == 'Act') {
                                                    ?>
                                                    <input type="hidden" id="actividad" name="actividad" value="Actividad">
                                                    <span><b>Fecha:</b></span><br>
                                                    <input onchange="diaSemana();" type="date" name="fromd" for="fromd"
                                                           id="fromd" value="" class="form-control inputstylo">
                                                    <input type="hidden" name="dia_sem" id="dia_sem">
                                                    <span><b>Hora Inicial:</b></span>
                                                    <input type="time" name="fromc" id="fromc" value=""
                                                           class="form-control inputstylo">
                                                    <span><b>Hora Final:</b></span>
                                                    <input type="time" name="toc" id="toc" value=""
                                                           class="form-control inputstylo">
                                                <?php }elseif ($act == 'Rep'){
                                                ?>
                                                <input type="hidden" id="actividad" name="actividad" value="Repetitiva">
                                                <span><b>Fecha Inicial:</b></span><br>
                                                <input type="date" name="fromd" for="fromd" id="fromd" value="" class="form-control inputstylo">
                                                <span><b>Hora Inicial:</b></span>
                                                <input type="time" name="fromc" id="fromc" value="" class="form-control inputstylo">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td>
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Fecha Final:</b></span><br>
                                                <input type="date" name="tod" for="tod" id="tod" value=""
                                                       class="form-control inputstylo">
                                                <span><b>Hora Final:</b></span>
                                                <input type="time" name="toc" id="toc" value=""
                                                       class="form-control inputstylo">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Dias a repetir:</b></span>
                                                <div class="modal-body">
                                            <span>
                                            <label class="checkbox"><input type="checkbox" name="domingo" value="1"> domingo</label>
                                            <label class="checkbox"><input type="checkbox" name="lunes" value="1"> lunes</label>
                                            <label class="checkbox"><input type="checkbox" name="martes" value="1"> martes</label>
                                            <label class="checkbox"><input type="checkbox" name="miercoles" value="1"> miercoles</label>
                                            <label class="checkbox"><input type="checkbox" name="jueves" value="1"> jueves</label>
                                            <label class="checkbox"><input type="checkbox" name="viernes" value="1"> viernes</label>
                                            <label class="checkbox"><input type="checkbox" name="sabado" value="1"> sabado</label>
                                        </span>
                                                    <?php
                                                    }elseif ($act == 'Inf'){
                                                    ?>
                                                    <input type="hidden" id="actividad" name="actividad" value="Infraestructura">
                                                    <span><b>Fecha Inicial De La Obra:</b></span><br>
                                                    <input type="date" name="fromd" for="fromd" id="fromd"
                                                           value="29/03/2018" class="form-control inputstylo">
                                                    <span><b>Hora Inicial:</b></span>
                                                    <input type="time" name="fromc" id="fromc" value="16:32"
                                                           class="form-control inputstylo">
                                                </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Fecha Final De La Obra:</b></span><br>
                                                <input type="date" name="tod" for="tod" id="tod" value="29/03/2018"
                                                       class="form-control inputstylo">
                                                <span><b>Hora Final:</b></span>
                                                <input type="time" name="toc" id="toc" value="16:32"
                                                       class="form-control inputstylo">
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <script>$("select").bsMultiSelect();</script>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Estado del Evento:</b></span>
                                                <select name="class" id="class" class="custom-select inputstylo">
                                                    <option selected>Seleccione el estado actual del evento:</option>
                                                    <option style="background-color: blue" class="text-white"
                                                            value="event-info">Terminando
                                                    </option>
                                                    <option style="background-color: green" value="event-success">
                                                        Ultimos preparativos
                                                    </option>
                                                    <option style="background-color: yellow" value="event-warning">En
                                                        Proceso
                                                    </option>
                                                    <option style="background-color: orange" value="event-important">
                                                        Necesita ser atendido
                                                    </option>
                                                    <option style="background-color: red" value="event-dark">Peligro
                                                        necesita ser atendido
                                                    </option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2"
                                                 style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Descripcion del evento:</b></span>
                                                <textarea id="body" name="event" required rows="3"
                                                          class="form-control inputstylo"
                                                          placeholder="Indroduce la descripcion del Evento"></textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Archivo de lista de asistecnia:</b></span>
                                                <input type="file" class="form-control-file btn btn-secondary" name="archi" id="archi" accept="image/jpeg,image/x-png,application/msword,application/msexcel,application/pdf">
                                                <!-- <input class="input" type="file" name="id_lista" accept="image/*,application/msword,application/msexcel,application/pdf" /> -->
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <button type="submit" class="btn btn-outline-success btn-block"><i
                                                        class="fas fa-check-circle"></i>Guardar
                                            </button>
                                            <a class="btn btn-outline-danger btn-block" href="../view.php"><i
                                                        class="fas fa-times" role="button"></i> Regresar </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>

        </div>
        <script>
            let dias = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
            let meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];

            function diaSemana() {
                var x = document.getElementById("fecha");
                let date = new Date(fromd.value.replace(/-+/g, '/'));
                var fechaNum = date.getDate();
                var mes_name = date.getMonth();
                document.getElementById("dia_sem").value = (dias[date.getDay() - 1]);
            }
        </script>
        <script>
            $('#framework_form').on('submit', function (event) {
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax({
                    url: "cbd.php",
                    method: "POST",
                    data: form_data,
                    success: function (data) {
                        $('#framework option:selected').each(function () {
                            $(this).prop('seleccionadas', false);
                        });
                        $('#framework').multiselect('refresh');
                        alert(data);
                    }
                });
            });
        </script>
    </div>
    </div>
    </div>
    </div>
    </body>

    </html>
    <?php
} else {
    echo '<script> window.location="../view.php"; </script>';
}
?>
