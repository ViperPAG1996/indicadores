<?php
session_start();
include '../conexion/config.php';
include '../calendario/funciones.php';
error_reporting(~E_NOTICE);
require_once '../conexion/dbconfig.php';

if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
    $id = $_GET['edit_id'];
    $stmt_edit = $DB_con->prepare('SELECT * FROM tbl_actividades WHERE id =:uid');
    $stmt_edit->execute(array(':uid' => $id));
    $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
    extract($edit_row);
} else {
    header("Location: ../view.php");
}
if (isset($_POST['btn_save_updates'])) {
    $actividad = $_POST['actividad'];
    if ($actividad == "Actividad"){
        //Fecha y hora inicial
        //Fecha
        $iniciod = date_create($_POST['fromd']);
        //Hora
        $inicioc = $_POST['fromc'];
        //Fecha y hora del calendario
        $newfechai = date_format($iniciod, "d/m/Y");
        $iniciot = $newfechai . " " . $inicioc;
        $inicio = _formatear($iniciot);
        $inicio_normal = $iniciot;
        //Fecha y hora final
        $finald = $iniciod;
        $finalc = $_POST['toc'];
        //Fecha y hora del calendario
        $newfechaf = date_format($finald, "d/m/Y");
        $finalt = $newfechaf . " " . $finalc;
        $final = _formatear($finalt);
        $final_normal = $finalt;
        //Dias
        $dias = $_POST['dia_sem'];
    }elseif ($actividad == "Repetitiva") {
        //Fecha y hora inicial
        //Fecha
        $iniciod = date_create($_POST['fromd']);
        //Hora
        $inicioc = $_POST['fromc'];
        $newfechai = date_format($iniciod, "d/m/Y");
        $iniciot = $newfechai . " " . $inicioc;
        $inicio = _formatear($iniciot);
        $inicio_normal = $iniciot;
        //Fecha y hora final
        //Fecha
        $finald = date_create($_POST['tod']);
        //Hora
        $finalc = $_POST['toc'];
        $newfechaf = date_format($finald, "d/m/Y");
        $finalt = $newfechaf . " " . $finalc;
        $final = _formatear($finalt);
        $final_normal = $finalt;
        //Dias
        $dias = "";
        if (isset($_REQUEST['domingo'])) {
            $d = 1;
            $dias = $dias . " domingo";
        } else {
            $d = 0;
        }
        if (isset($_REQUEST['lunes'])) {
            $l = 1;
            $dias = $dias . " lunes";
        } else {
            $l = 0;
        }
        if (isset($_REQUEST['martes'])) {
            $m = 1;
            $dias = $dias . " martes";
        } else {
            $m = 0;
        }
        if (isset($_REQUEST['miercoles'])) {
            $mi = 1;
            $dias = $dias . " miercoles";
        } else {
            $mi = 0;
        }
        if (isset($_REQUEST['jueves'])) {
            $j = 1;
            $dias = $dias . " jueves";
        } else {
            $j = 0;
        }
        if (isset($_REQUEST['viernes'])) {
            $v = 1;
            $dias = $dias . " viernes";
        } else {
            $v = 0;
        }
        if (isset($_REQUEST['sabado'])) {
            $s = 1;
            $dias = $dias . " sabado";
        } else {
            $s = 0;
        }
    }elseif ($actividad == "Infraestructura") {
        //Fecha y hora inicial
        //Fecha
        $iniciod = date_create($_POST['fromd']);
        //Hora
        $inicioc = $_POST['fromc'];
        $newfechai = date_format($iniciod, "d/m/Y");
        $iniciot = $newfechai . " " . $inicioc;
        $inicio = _formatear($iniciot);
        $inicio_normal = $iniciot;
        //Fecha y hora final
        //Fecha
        $finald = date_create($_POST['tod']);
        //Hora
        $finalc = $_POST['toc'];
        $newfechaf = date_format($finald, "d/m/Y");
        $finalt = $newfechaf . " " . $finalc;
        $final = _formatear($finalt);
        $final_normal = $finalt;
        //Dias
        $dias = "";
    }
    //Titulo
    $titulo = evaluar($_POST['title']);
    //Descripcion
    $body = evaluar($_POST['event']);
    //Estado
    $clase = evaluar($_POST['class']);
    //Factor
    if (isset($_POST["factor"])) {
        $factor = '';
        foreach ($_POST["factor"] as $row) {
            $factor .= $row . ', ';
        }
        $factor = substr($factor, 0, -2);
    }
    //Tipo de actividad
    if (isset($_POST["tipo"])) {
        $tipo = '';
        foreach ($_POST["tipo"] as $row) {
            $tipo .= $row . ', ';
        }
        $tipo = substr($tipo, 0, -2);
    }
    //Dependencias
    if (isset($_POST["framework"])) {
        $framework = '';
        foreach ($_POST["framework"] as $row) {
            $framework .= $row . ', ';
        }
        $framework = substr($framework, 0, -2);
    }
    //Coordenadas del mapa
    $lon = $_POST['lon'];
    $lat = $_POST['lat'];
    //Lugar
    $lugar = $_POST['lugar'];
    //Creador del evento
    $creador = $_SESSION['user'];
    //Monto
    $monto = $_POST['monto'];
    $url="";
    if ($_POST['monto'] == "") {
        $monto = 0;
    }
    //Personas beneficiadas
    $personas = $_POST['personas'];
    if ($_POST['personas'] == "") {
        $personas = " ";
    }
    // if no error occured, continue ....
    if (!isset($errMSG)) {
        $stmt = $DB_con->prepare('UPDATE tbl_actividades
										     SET title=:tit,
											     factor=:fa,
											     tipo=:ti,
	                                            dependencia=:dep,
										         lugar=:lug,
										         lon=:lo,
										         lat=:la,
										         monto=:mon,
										         personas=:per,
										         body=:bo,
										         url=:ur,
										         class=:cl,
										         start=:sta,
										         end=:en,
										         inicio_normal=:in,
										         final_normal=:fn,
										         dias=:das
	                                      WHERE id=:uid');
        $stmt->bindParam(':tit', $title);
        $stmt->bindParam(':fa', $factor);
        $stmt->bindParam(':ti', $tipo);
        $stmt->bindParam(':dep', $framework);
        $stmt->bindParam(':lug', $lugar);
        $stmt->bindParam(':lo', $lon);
        $stmt->bindParam(':la', $lat);
        $stmt->bindParam(':mon', $monto);
        $stmt->bindParam(':per', $personas);
        $stmt->bindParam(':bo', $body);
        $stmt->bindParam(':ur', $url);
        $stmt->bindParam(':cl', $clase);
        $stmt->bindParam(':sta', $inicio);
        $stmt->bindParam(':en', $final);
        $stmt->bindParam(':in', $inicio_normal);
        $stmt->bindParam(':fn', $final_normal);
        $stmt->bindParam(':das', $dias);
        $stmt->bindParam(':uid', $id);
        if ($stmt->execute()) {
            // Obtenemos el ultimo id insetado
            $im = $conexion->query("SELECT id FROM tbl_actividades where id=$id");
            $row = $im->fetch_row();
            $id = trim($row[0]);
            // para generar el link del evento
            $link = "$base_url" . "calendario/descripcion_evento.php?id=$id";
            // y actualizamos su link
            $query = "UPDATE tbl_actividades SET url = '$link' WHERE id = $id";
            // Ejecutamos nuestra sentencia sql
            $conexion->query($query);

            if ($conexion->query($query) === true) {
                echo "<script>
                alert('El articulo se actualizo con exito...');
                window.location.href = '../view.php';
            </script>";
            }else{
                echo $conexion->error;
            }

        } else {
            $errMSG = "Los datos no fueron actualizados !";
        }
    }
}
?>
.
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
    <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
    <link rel="icon" href="../imagenes/ico.png">

    <title>Crear nuevo Indicador</title>

    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <script src="../bootstrap/js/jquery.min.js"></script>
    <script src="../bootstrap/js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
            integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
            crossorigin=""></script>
    <style>
        .inputstylo:focus {
            background-color: #E9ECEF;
            border: none;
            border-radius: 15px;
            outline: none;
            box-shadow: none;
        }

        .inputstylo {
            background-color: #E9ECEF;
            border: none;
            border-radius: 15px;
        }

        .dropdown-menu {
            max-height: 350px;
            overflow-y: auto;
            overflow-x: hidden;
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css"/>

    <script src="../dist/js/BsMultiSelect.js"></script>
    <link rel="stylesheet" href="../dist/css/BsMultiSelect.css">
</head>

<body style="background-color: #E9ECEF">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">Subsecretaría </a>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="navbar-nav ml-md-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                               data-toggle="dropdown"><?php echo $_SESSION['user']; ?></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="calendario/calendario.php">Ver Calendario</a>
                                <a class="dropdown-item" href="graficas/graficas.php">Ver Graficas</a>
                                <a class="dropdown-item" href="usuarios/usermod.php">Manejo de usuarios</a>
                                <div class="dropdown-divider">
                                </div>
                                <a class="dropdown-item" href="../logout.php">Cerrar Sesión</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="row">
                <main role="main" class="container">
                    <form method="POST" enctype="multipart/form-data" class="form-horizontal">
                        <?php
                        if (isset($errMSG)) {
                            ?>
                            <div class="alert alert-danger">
                                <span class="glyphicon glyphicon-info-sign"></span> &nbsp;
                                <?php echo $errMSG; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="page-header p-3">
                            <br>
                            <br>
                            <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                <h6 class="mb-0 text-white lh-100">Modificar</h6>
                            </div>
                        </div>
                        <div class="p-3 bg-white rounded shadow-sm">

                            <table class="table table-borderless table-sm">
                                <tbody>

                                <tr>
                                    <td>
                                        <div class="form-group border  p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Titulo (Linea Base Indicador):</b></span>

                                            <input type="text" class="form-control inputstylo" name="title" id="title"
                                                   for="title" value="<?php echo $title; ?>"
                                                   class="form-control inputstylo">

                                        </div>

                                    </td>
                                    <td width="50%" rowspan="8">
                                        <center>
                                            <input id="lon" name="lon" value="-102.58317380212249">
                                            <input id="lat" name="lat" value="22.767575819739957">

                                            <div class="input" id="buscador">
                                                <input type="text" class="form-control inputstylo" id="lugar"
                                                       name="lugar"
                                                       placeholder="Ingrese Direccion y/o busque en el mapa">
                                                <button class="btn btn-outline-secondary btn-block" type="button"
                                                        onclick="direccion_buscador();">Buscar
                                                </button>
                                                <div id="resultado"/>
                                            </div>
                                            <div id='map' style="height:40vw; width: 100%;"></div>

                                        </center>

                                        <script>
                                            var map = L.map('map').setView([22.767575819739957, -102.58317380212249], 10);
                                            // Capas base
                                            var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                                                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap<\/a> contributors'

                                            }).addTo(map);

                                            var feature;

                                            function elegirDireccion(lat1, lng1, lat2, lng2, tipo_osm) {
                                                var loc1 = new L.LatLng(lat1, lng1);
                                                var loc2 = new L.LatLng(lat2, lng2);
                                                var bounds = new L.LatLngBounds(loc1, loc2);

                                                if (feature) {
                                                    map.removeLayer(feature);
                                                }
                                                if (tipo_osm == "node") {
                                                    feature = L.circle(loc1, 25, {
                                                        color: 'green',
                                                        fill: false
                                                    }).addTo(map);
                                                    map.fitBounds(bounds);
                                                    map.setZoom(18);
                                                } else {
                                                    var loc3 = new L.LatLng(lat1, lng2);
                                                    var loc4 = new L.LatLng(lat2, lng1);

                                                    //  feature = L.polyline( [loc1, loc4, loc2, loc3, loc1], {color: 'red'}).addTo(map);
                                                    map.fitBounds(bounds);
                                                }
                                            }

                                            function direccion_buscador() {
                                                var entrada = document.getElementById("lugar");

                                                $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=5&q=' + entrada.value, function (data) {
                                                    var items = [];

                                                    $.each(data, function (key, val) {
                                                        bb = val.boundingbox;
                                                        items.push("<li><a href='#' onclick='elegirDireccion(" + bb[0] + ", " + bb[2] + ", " + bb[1] + ", " + bb[3] + ", \"" + val.tipo_osm + "\");return false;'>" + val.display_name + '</a></li>');
                                                    });

                                                    $('#resultado').empty();
                                                    if (items.length != 0) {
                                                        $('<p>', {html: "Resultados de la b&uacute;queda:"}).appendTo('#resultado');
                                                        $('<ul/>', {
                                                            'class': 'my-new-list',
                                                            html: items.join('')
                                                        }).appendTo('#resultado');
                                                    } else {
                                                        $('<p>', {html: "Ningun resultado encontrado."}).appendTo('#resultado');
                                                    }
                                                });
                                            }

                                            var popup = L.popup();

                                            function simpleReverseGeocoding(lon, lat) {
                                                fetch('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + lon + '&lat=' + lat).then(function (response) {
                                                    return response.json();
                                                }).then(function (json) {

                                                    var lugar = document.getElementById('lugar').value = json.display_name.toString();
                                                    //var lugar2 = document.getElementById('lugar2').value = json.display_name;
                                                })
                                            }

                                            var punto = L.marker([22.767575819739957, -102.58317380212249]);
                                            punto.addTo(map);
                                            punto.bindPopup('Evento').openPopup();

                                            function onMapClick(e) {

                                                var lon = document.getElementById('lon').value = e.latlng.lng.toString();
                                                var lat = document.getElementById('lat').value = e.latlng.lat.toString();
                                                simpleReverseGeocoding(lon, lat);
                                                punto.remove();

                                                popup


                                                    .setLatLng(e.latlng) // Sets the geographical point where the popup will open.
                                                    .setContent('<center><i class="far fa-dot-circle fa-2x"></i></center>') // Sets the HTML content of the popup.
                                                    .openOn(map); // Adds the popup to the map and closes the previous one.

                                            }

                                            map.on('click', onMapClick);
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Factor de Riesgo:</b></span>
                                            <select name="factor[]" id="factor" class="custom-select inputstylo"
                                                    multiple="multiple">
                                                <!--                                                          <option selected>-->
                                                <!--                                                              --><?php
                                                //                                                              $factor=$factor.',';
                                                //                                                              do{
                                                //                                                                  $factor1 = strstr($factor, ',', true);
                                                //                                                                  $factor2 = strstr($factor, ',');
                                                //                                                                  echo $factor1;
                                                //                                                                  $factor=substr($factor2,2);
                                                //                                                              }while($factor!='');
                                                //                                                              ?>
                                                <?php
                                                $EX = 0;
                                                $PC = 0;
                                                $DE = 0;
                                                $EJ = 0;
                                                $PE = 0;
                                                $PCI = 0;
                                                $AD = 0;
                                                $factor = $factor . ',';
                                                do {
                                                    //echo '<option selected>';
                                                    $factor1 = strstr($factor, ',', true);
                                                    $factor2 = strstr($factor, ',');
                                                    //echo '<p style="background-color: " value="event-info">';
                                                    //echo $factor1;
                                                    if ($factor1 == "Exclusión Social Del Entorno Urbano") {
                                                        $EX = 1;
                                                    }
                                                    if ($factor1 == "Pobreza Cultural") {
                                                        $PC = 1;
                                                    }
                                                    if ($factor1 == "Deserción Escolar") {
                                                        $DE = 1;
                                                    }
                                                    if ($factor1 == "Embarazos Juveniles") {
                                                        $EJ = 1;
                                                    }
                                                    if ($factor1 == "Precarización Del Empleo") {
                                                        $PE = 1;
                                                    }
                                                    if ($factor1 == "Poca Confianza En Las Instituciones") {
                                                        $PCI = 1;
                                                    }
                                                    if ($factor1 == "Adicciones") {
                                                        $AD = 1;
                                                    }
                                                    //echo '</p>';
                                                    $factor = substr($factor2, 2);
                                                    //echo '</option>';
                                                } while ($factor != '');
                                                ?>
                                                <option style="background-color: white"
                                                        value="Exclusión Social Del Entorno Urbano" <?php if ($EX == 1) {
                                                    echo "selected";
                                                } ?>>Exclusión Social Del Entorno Urbano
                                                </option>
                                                <option style="background-color: white"
                                                        value="Pobreza Cultural" <?php if ($PC == 1) {
                                                    echo "selected";
                                                } ?>>Pobreza Cultural
                                                </option>
                                                <option style="background-color: white"
                                                        value="Deserción Escolar" <?php if ($DE == 1) {
                                                    echo "selected";
                                                } ?>>Deserción Escolar
                                                </option>
                                                <option style="background-color: white"
                                                        value="Embarazos Juveniles" <?php if ($EJ == 1) {
                                                    echo "selected";
                                                } ?>>Embarazos Juveniles
                                                </option>
                                                <option style="background-color: white"
                                                        value="Precarización Del Empleo" <?php if ($PE == 1) {
                                                    echo "selected";
                                                } ?>>Precarización Del Empleo
                                                </option>
                                                <option style="background-color: white"
                                                        value="Poca Confianza En Las Instituciones" <?php if ($PCI == 1) {
                                                    echo "selected";
                                                } ?>>Poca Confianza En Las Instituciones
                                                </option>
                                                <option style="background-color: white"
                                                        value="Adicciones" <?php if ($AD == 1) {
                                                    echo "selected";
                                                } ?>>Adicciones
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Tipo de Actividad:</b></span>
                                            <select name="tipo[]" id="tipo" class="custom-select inputstylo"
                                                    multiple="multiple">
                                                <!--                                                          <option selected>-->
                                                <!--                                                              --><?php
                                                //                                                              $factor=$factor.',';
                                                //                                                              do{
                                                //                                                                  $factor1 = strstr($factor, ',', true);
                                                //                                                                  $factor2 = strstr($factor, ',');
                                                //                                                                  echo $factor1;
                                                //                                                                  $factor=substr($factor2,2);
                                                //                                                              }while($factor!='');
                                                //                                                              ?>
                                                <?php
                                                $cultura = 0;
                                                $empleo = 0;
                                                $deportes = 0;
                                                $adicciones = 0;
                                                $urbanismo = 0;
                                                $tipo = $tipo . ',';
                                                do {
                                                    //echo '<option selected>';
                                                    $tipo1 = strstr($tipo, ',', true);
                                                    $tipo2 = strstr($tipo, ',');
                                                    //echo '<p style="background-color: " value="event-info">';
                                                    //echo $factor1;
                                                    if ($tipo1 == "Cultura") {
                                                        $cultura = 1;
                                                    }
                                                    if ($tipo1 == "Empleo") {
                                                        $empleo = 1;
                                                    }
                                                    if ($tipo1 == "Deportes") {
                                                        $deportes = 1;
                                                    }
                                                    if ($tipo1 == "Adicciones") {
                                                        $adicciones = 1;
                                                    }
                                                    if ($tipo1 == "Urbanismo(Obras)") {
                                                        $urbanismo = 1;
                                                    }
                                                    //echo '</p>';
                                                    $tipo = substr($tipo2, 2);
                                                    //echo '</option>';
                                                } while ($tipo != '');
                                                ?>
                                                <option style="background-color: blue"
                                                        value="Cultura" <?php if ($cultura == 1) {
                                                    echo "selected";
                                                } ?>>Cultura
                                                </option>
                                                <option style="background-color: green"
                                                        value="Empleo" <?php if ($empleo == 1) {
                                                    echo "selected";
                                                } ?>>Empleo
                                                </option>
                                                <option style="background-color: yellow"
                                                        value="Deportes" <?php if ($deportes == 1) {
                                                    echo "selected";
                                                } ?>>Deportes
                                                </option>
                                                <option style="background-color: orange"
                                                        value="Adicciones" <?php if ($adicciones == 1) {
                                                    echo "selected";
                                                } ?>>Adicciones
                                                </option>
                                                <option style="background-color: red"
                                                        value="Urbanismo(Obras)" <?php if ($urbanismo == 1) {
                                                    echo "selected";
                                                } ?>>Urbanismo(Obras)
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <form method="post" id="framework_form">
                                                <div class="form-group">
                                                <span><b>Dependencias Involucradas:</b></span>
                                                <select name="framework[]" id="framework"
                                                        class="form-control inputstylo" multiple="multiple">
                                                    <?php
                                                    $dependencia = $dependencia . ',';
                                                    do {
                                                        $dependencia1 = strstr($dependencia, ',', true);
                                                        $dependencia2 = strstr($dependencia, ',');
                                                        $dependencia = substr($dependencia2, 2);
                                                        $arrdep [] = $dependencia1;
                                                    } while ($dependencia != '');
                                                    var_dump($arrdep);
                                                    $stmt = $DB_con->prepare('SELECT * FROM tbl_dep');
                                                    $stmt->execute();
                                                    if ($stmt->rowCount() > 0) {
                                                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                            extract($row);
                                                            if (in_array($row['user'], $arrdep)) {
                                                                ?>
                                                                <option id="<?php echo $row['id']; ?>"
                                                                        name="<?php echo $row['user']; ?>"
                                                                        value="<?php echo $row['user']; ?>"
                                                                        selected><?php echo $row['user']; ?></option>
                                                                <?php
                                                            } else {
                                                                ?>

                                                                <option id="<?php echo $row['id']; ?>"
                                                                        name="<?php echo $row['user']; ?>"
                                                                        value="<?php echo $row['user']; ?>"><?php echo $row['user']; ?></option>

                                                                <?php
                                                            }
                                                        }

                                                    } else {
                                                        ?>
                                                        <div class="col-xs-12">
                                                            <div class="alert alert-warning">
                                                                <span class="glyphicon glyphicon-info-sign"></span>
                                                                &nbsp; Datos no encontrados ...
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <?php
                                            if ($actividad == 'Actividad') {
                                                ?>
                                                <span><b>Fecha:</b></span><br>
                                                <input type="hidden" id="actividad" name="actividad" value="Actividad">
                                                <?php
                                                $year = substr($inicio_normal, 6, 4) ;
                                                $mes =  substr($inicio_normal, 3, 2);
                                                $day =  substr($inicio_normal, 0, 2);
                                                ?>
                                                <input onchange="diaSemana();" type="date" name="fromd" for="fromd"
                                                       id="fromd"
                                                       value="<?php echo $year."-".$mes."-".$day ?>"
                                                       class="form-control inputstylo">
                                                <input type="hidden" name="dia_sem" id="dia_sem">
                                                <span><b>Hora Inicial:</b></span>
                                                <input type="time" name="fromc" id="fromc" for="fromc"
                                                       value="<?php echo date(substr($inicio_normal, 11, 16)); ?>"
                                                       class="form-control inputstylo">
                                                <span><b>Hora Final:</b></span>
                                                <input type="time" name="toc" id="toc" for="toc"
                                                       value="<?php echo date(substr($final_normal, 11, 16)); ?>"
                                                       class="form-control inputstylo">

                                            <?php }elseif ($actividad == 'Repetitiva'){
                                            ?>
                                            <span><b>Fecha Inicial:</b></span><br>
                                            <input type="hidden" id="actividad" name="actividad" value="Repetitiva">
                                            <?php
                                            $year = substr($inicio_normal, 6, 4) ;
                                            $mes =  substr($inicio_normal, 3, 2);
                                            $day =  substr($inicio_normal, 0, 2);
                                            ?>
                                            <input type="date" name="fromd" for="fromd" id="fromd"
                                                   value="<?php echo $year."-".$mes."-".$day ?>"
                                                   class="form-control inputstylo">
                                            <span><b>Hora Inicial:</b></span>
                                            <input type="time" name="fromc" for="fromc" id="fromc"
                                                   value="<?php echo date(substr($inicio_normal, 11, 16)); ?>"
                                                   class="form-control inputstylo">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Fecha Final:</b></span><br>
                                            <?php
                                            $year = substr($final_normal, 6, 4) ;
                                            $mes =  substr($final_normal, 3, 2);
                                            $day =  substr($final_normal, 0, 2);
                                            ?>
                                            <input type="date" name="tod" for="tod" id="tod"
                                                   value="<?php echo $year."-".$mes."-".$day ?>"
                                                   class="form-control inputstylo">
                                            <span><b>Hora Final:</b></span>
                                            <input type="time" name="toc" for="toc" id="toc"
                                                   value="<?php echo date(substr($final_normal, 11, 16)); ?>"
                                                   class="form-control inputstylo">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                                <?php
                                                $lunes = 0;
                                                $martes = 0;
                                                $miercoles = 0;
                                                $jueves = 0;
                                                $viernes = 0;
                                                $sabado = 0;
                                                $domingo = 0;
                                                $dias = $dias . ' ';
                                                do {
                                                    //echo '<option selected>';
                                                    $dias1 = strstr($dias, ' ', true);
                                                    $dias2 = strstr($dias, ' ');
                                                    //echo '<p style="background-color: " value="event-info">';
                                                    //echo $factor1;
                                                    if ($dias1 == "lunes") {
                                                        $lunes = 1;
                                                    }
                                                    if ($dias1 == "martes") {
                                                        $empleo = 1;
                                                    }
                                                    if ($dias1 == "miercoles") {
                                                        $miercoles = 1;
                                                    }
                                                    if ($dias1 == "jueves") {
                                                        $jueves = 1;
                                                    }
                                                    if ($dias1 == "viernes") {
                                                        $viernes = 1;
                                                    }
                                                    if ($dias1 == "sabado") {
                                                        $sabado = 1;
                                                    }
                                                    if ($dias1 == "domingo") {
                                                        $domingo = 1;
                                                    }
                                                    //echo '</p>';
                                                    $dias = substr($dias2, 1);
                                                    //echo '</option>';
                                                } while ($dias != '');
                                                ?>
                                            <span><b>Dias a repetir:</b></span>
                                            <div class="modal-body">
                                            <span>
                                            <label class="checkbox"><input type="checkbox" name="domingo" value="1" <?php if ($domingo == 1) {echo "checked";}?>>domingo</label>
                                            <label class="checkbox"><input type="checkbox" name="lunes" value="1" <?php if ($lunes == 1) {echo "checked";}?>>lunes</label>
                                            <label class="checkbox"><input type="checkbox" name="martes" value="1" <?php if ($martes == 1) {echo "checked";}?>>martes</label>
                                            <label class="checkbox"><input type="checkbox" name="miercoles" value="1" <?php if ($miercoles == 1) {echo "checked";}?>>miercoles</label>
                                            <label class="checkbox"><input type="checkbox" name="jueves" value="1" <?php if ($jueves == 1) {echo "checked";}?>>jueves</label>
                                            <label class="checkbox"><input type="checkbox" name="viernes" value="1" <?php if ($viernes == 1) {echo "checked";}?>>viernes</label>
                                            <label class="checkbox"><input type="checkbox" name="sabado" value="1" <?php if ($sabado == 1) {echo "checked";}?>>sabado</label>
                                        </span>
                                            <?php
                                            }elseif ($actividad == 'Infraestructura'){
                                            ?>
                                            <input type="hidden" id="actividad" name="actividad" value="Infraestructura">
                                            <span><b>Fecha Inicial De La Obra:</b></span><br>
                                                <?php
                                                $year = substr($inicio_normal, 6, 4) ;
                                                $mes =  substr($inicio_normal, 3, 2);
                                                $day =  substr($inicio_normal, 0, 2);
                                                ?>
                                            <input type="date" name="fromd" for="fromd" id="fromd"
                                                   value="<?php echo $year."-".$mes."-".$day ?>"
                                                   class="form-control inputstylo">
                                            <span><b>Hora Inicial:</b></span>
                                            <input type="time" name="fromc" for="fromc" id="fromc"
                                                   value="<?php echo date(substr($inicio_normal, 11, 16)); ?>"
                                                   class="form-control inputstylo">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Fecha Final De La Obra:</b></span><br>
                                            <?php
                                                $year = substr($final_normal, 6, 4) ;
                                                $mes =  substr($final_normal, 3, 2);
                                                $day =  substr($final_normal, 0, 2);
                                            ?>
                                            <input type="date" name="tod" for="tod" id="tod"
                                                   value="<?php echo $year."-".$mes."-".$day ?>"
                                                   class="form-control inputstylo">
                                            <span><b>Hora Final:</b></span>
                                            <input type="time" name="toc" for="toc" id="toc"
                                                   value="<?php echo date(substr($final_normal, 11, 16)); ?>"
                                                   class="form-control inputstylo">
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                <script>$("select").bsMultiSelect();</script>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Estado del Evento:</b></span>
                                            <select name="class" id="class" class="custom-select inputstylo">
                                                <option style="background-color: blue" value="event-info"
                                                        value="event-info" <?php if ($class == 'event-info') {echo "selected";}?> >Terminando
                                                </option>
                                                <option style="background-color: green" value="event-success" <?php if ($class == 'event-success') {echo "selected";}?> >Ultimos
                                                    preparativos
                                                </option>
                                                <option style="background-color: yellow" value="event-warning" <?php if ($class == 'event-warning') {echo "selected";}?>>En
                                                    Proceso
                                                </option>
                                                <option style="background-color: orange" value="event-important" <?php if ($class == 'event-important') {echo "selected";}?>>
                                                    Necesita ser atendido
                                                </option>
                                                <option style="background-color: red" value="event-dark" <?php if ($class == 'event-dark') {echo "selected";}?>>Abandonado
                                                    necesita ser atendido
                                                </option>

                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group border p-2"
                                             style="background-color: #E9ECEF;border-radius: 15px;">
                                            <span><b>Descripcion del evento:</b></span>
                                            <p class="p-1">
                                                <textarea name="event" id="event"><?php echo $body; ?></textarea>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <button type="submit" name="btn_save_updates" class="btn btn-success btn-block"><i
                                    class="fas fa-check-circle"></i>Modificar
                        </button>
                        <a class="btn btn-danger btn-block" href="../view.php" role="button"><i
                                    class="fas fa-times"></i>Regresar</a>
                        <br>
                    </form>
                </main>
            </div>
        </div>
    </div>
    <script>
        let dias = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
        let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        function diaSemana() {
            var x = document.getElementById("fecha");
            let date = new Date(fromd.value.replace(/-+/g, '/'));
            var fechaNum = date.getDate();
            var mes_name = date.getMonth();
            document.getElementById("dia_sem").value = (dias[date.getDay() - 1]);
        }
    </script>
    <script>
        $('#framework_form').on('submit', function (event) {
            event.preventDefault();
            var form_data = $(this).serialize();
            $.ajax({
                url: "cbd.php",
                method: "POST",
                data: form_data,
                success: function (data) {
                    $('#framework option:selected').each(function () {
                        $(this).prop('seleccionadas', false);
                    });
                    $('#framework').multiselect('refresh');
                    alert(data);
                }
            });
        });
        })
        ;
    </script>
</div>
</div>

</body>

</html>
