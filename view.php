<?php
session_start();
include 'conexion/config.php';
include 'conexion/dbconfig.php';

if (isset($_GET['delete_id'])) {
    $stmt_delete = $DB_con->prepare('DELETE FROM tbl_actividades WHERE id =:uid');
    $stmt_delete->bindParam(':uid', $_GET['delete_id']);
    $stmt_delete->execute();
    header("Location: view.php");
}

if (isset($_SESSION['user'])) {
    $usuario = $_SESSION['user'];
    $log = $conexion->query("SELECT * FROM tbl_usuarios WHERE user='$usuario'");
    if (mysqli_num_rows($log) > 0) {
        $row = mysqli_fetch_array($log);
        $nivel = $row['nivel'];
        if ($nivel == 1 or $nivel == 2) {
            ?>
            <!DOCTYPE html>
            <html lang="es">

            <head>
                <meta name="author" content="Pedro Aguilar Guerrero ITZ-ISC 2018">
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport"
                      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
                <meta name="description"
                      content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
                <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
                <link rel="icon" href="../imagenes/ico.png">

                <title>Indicadors Delictivos</title>

                <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
                <script src="bootstrap/js/jquery.min.js"></script>
                <script src="bootstrap/js/popper.min.js"></script>
                <script src="bootstrap/js/bootstrap.min.js"></script>

                <link href="bootstrap/css/fontawesome-all.css" rel="stylesheet">
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
                      integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
                      crossorigin=""/>
                <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
                        integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
                        crossorigin=""></script>
                <style>
                </style>
                <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
                <script type="text/javascript" src="DataTables/datatables.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#lookup').DataTable({
                            "language": {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ registros",
                                "sZeroRecords": "No se encontraron resultados",
                                "sEmptyTable": "Ningún dato disponible en esta tabla",
                                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar:",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Último",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                        });

                    });
                </script>
            </head>
            <body style="background-color: #E9ECEF">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="navbar-brand" href="http://localhost/src/index.php">Subsecretaría </a>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="navbar-nav ml-md-auto">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="http://example.com"
                                           id="navbarDropdownMenuLink"
                                           data-toggle="dropdown"><?php echo $_SESSION['user']; ?></a>
                                        <div class="dropdown-menu dropdown-menu-right"
                                             aria-labelledby="navbarDropdownMenuLink">
                                            <?php if($nivel == 1){
                                                echo '<a class="dropdown-item" href="calendario/calendario.php">Ver Calendario</a>
                                            <a class="dropdown-item" href="graficas/graficas.php">Ver Graficas</a>
                                            <a class="dropdown-item" href="usuarios/usermod.php">Manejo de usuarios</a>
                                            <div class="dropdown-divider">
                                            </div>';
                                            }?>
                                            <a class="dropdown-item" href="logout.php">Cerrar Sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-10">

                                <div class="page-header p-3">
                                    <br>
                                    <br>
                                    <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                        <h6 class="mb-0 text-white lh-100">Indicadores Delictivos Del Estado De
                                            Zacatecas <i class="fas fa-circle"></i></h6>
                                    </div>
                                </div>
                                <div class="p-3 bg-white rounded shadow-sm">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">Vista General</a>
                                        </li>
                                        <?php if($nivel == 1){
                                            echo '<li class="nav-item">
                                            <a class="nav-link" href="actividades/reportes.php">Reportes</a>
                                        </li>';
                                        } ?>
                                    </ul>
                                    <br>
                                    <!-- Button to Open the Modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#myModal">
                                        Agregar Nuevo Evento
                                    </button>

                                    <!-- The Modal -->
                                    <div class="modal fade" id="myModal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Seleccione tipo de evento:</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body" style="">
                                                    <a href="actividades/addnew.php?act=Rep" class="btn btn-dark"
                                                       role="button">Actividad Repetitiva</a>
                                                    <a href="actividades/addnew.php?act=Inf" class="btn btn-dark"
                                                       role="button">Infraestructura</a>
                                                    <a href="actividades/addnew.php?act=Act" class="btn btn-dark"
                                                       role="button">Actividad</a>
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                        Cerrar
                                                    </button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php if($nivel == 1){
                                        echo '<a href="calendario/calendario.php" class="btn btn-info" role="button">Ver
                                        Calendario</a>
                                        <a href="graficas/graficas.php" class="btn btn-success" role="button">Ver
                                        Graficas</a>
                                    <a href="usuarios/usermod.php" class="btn btn-dark" role="button">Manejo de
                                        usuarios</a>';
                                    } ?>

                                    <div class="media text-muted pt-3">
                                        <div class="table-responsive">
                                            <table id="lookup" class="table table-bordered table-sm">
                                                <thead bgcolor="#eeeeee" align="center">
                                                <tr>
                                                    <th>Accion</th>
                                                    <th>Factor</th>
                                                    <th>Tipo de Actividad</th>
                                                    <th>Dependencias Involucradas</th>
                                                    <th>Lugar</th>
                                                    <th>Descripcion</th>
                                                    <th>Estado de Actividad</th>
                                                    <th>Fecha de Inicio</th>
                                                    <th>Fecha de Terminacion</th>
                                                    <th>Dias</th>
                                                    <th class="text-center">Opciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if ($nivel == 1) {
                                                    $stmt = $DB_con->prepare('SELECT * FROM tbl_actividades ORDER BY id desc');
                                                } elseif ($nivel == 2) {
                                                    $stmt = $DB_con->prepare('SELECT * FROM tbl_actividades where creador="'.$usuario.'" or dependencia like "%'.$usuario.'%"');
                                                }
                                                $stmt->execute();
                                                if ($stmt->rowCount() > 0) {
                                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                        extract($row);
                                                        ?>
                                                        <tr>
                                                            <!-- <td><?php echo $row['id']; ?></td> -->
                                                            <td><?php echo $row['title']; ?></td>
                                                            <td><?php echo $row['factor']; ?></td>
                                                            <td><?php echo $row['tipo'] ?></td>
                                                            <td><?php echo $row['dependencia']; ?></td>
                                                            <td><?php echo $row['lugar']; ?></td>
                                                            <td><?php echo $row['body']; ?></td>
                                                            <?php
                                                            if ($class == 'event-info') {
                                                                echo '<td class="bg-info text-white">Terminado.</td>';
                                                            } elseif ($class == 'event-success') {
                                                                echo '<td class="bg-success text-white">Ultimos preparativos.</td>';
                                                            } elseif ($class == 'event-warning') {
                                                                echo '<td class="bg-warning text-white">En Proceso.</p>';
                                                            } elseif ($class == 'event-important') {
                                                                echo '<td class="bg-danger text-white">Necesita ser atendido.</td>';
                                                            } elseif ($class == 'event-dark') {
                                                                echo '<td class="bg-dark text-white">Abandonado necesita ser atendido.</td>';
                                                            } else {
                                                                echo '<td class="bg-light text-black">Error.</td>';
                                                            }
                                                            ?>
                                                            <td><?php echo $row['inicio_normal']; ?></td>
                                                            <td><?php echo $row['final_normal']; ?></td>
                                                            <td><?php echo $row['dias']; ?></td>
                                                            <td>
                    <span>
                    <div class="btn-group">
                    <a href="actividades/readform.php?read_id=<?php echo $row['id']; ?>" role="button" title="Ver"
                       class="btn btn-outline-warning"> <i class="fas fa-eye"></i> </a>
	                <a href="actividades/editform.php?edit_id=<?php echo $row['id']; ?>" role="button"
                       title="Editar datos" class="btn btn-outline-info"> <i class="fas fa-pencil-alt"></i> </a>
	                <a href="?delete_id=<?php echo $row['id']; ?>" role="button" title="Eliminar"
                       class="btn btn-outline-danger"> <i class="far fa-trash-alt"></i> </a>
                    </div>
                                                        </span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="alert alert-warning">
                                                            <span class="glyphicon glyphicon-info-sign"></span> &nbsp;
                                                            Datos no encontrados ...
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                </div>
            </div>
            </div>
            </body>
            </html>
            <?php
        }
    }
} else {
    echo '<script> window.location="index.php"; </script>';
}
?>
