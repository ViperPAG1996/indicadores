<?php
session_start();
include '../conexion/config.php';
include '../conexion/dbconfig.php';
if(isset($_SESSION['user'])) {?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta name="author" content="Pedro Aguilar Guerrero ITZ-ISC 2018">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
        <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
        <link rel="icon" href="../imagenes/ico.png">
        <title>Agregar Usuario</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <script src="../bootstrap/js/jquery.min.js"></script>
        <script src="../bootstrap/js/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
              crossorigin="" />
        <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
                crossorigin=""></script>
        <script type="text/javascript">
            function validar(){
                var txtAccion = document.getElementById('accion').value;
                var txtFactor = document.getElementById('factor').value;
                var txtDependencias = document.getElementById('dependencias').value;
                var txtFecha = document.getElementById('fecha_inicio').value;
                if(txtAccion == null || txtAccion.length == 0 || /^\s+$/.test(txtAccion)){
                    alert('ERROR: El campo accion no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if(txtFactor == null || txtFactor.length == 0 || /^\s+$/.test(txtFactor)){
                    alert('ERROR: El campo factor no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if(txtDependencias == null || txtDependencias.length == 0 || /^\s+$/.test(txtDependencias)){
                    alert('ERROR: El campo dep no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if(!isNaN(txtFecha)){
                    alert('ERROR: Debe elegir una fecha');
                    return false;
                }
                return true;
            }
        </script>
        <style>
            .inputstylo:focus {
                background-color: #E9ECEF;
                border: none;
                border-radius: 15px;
                outline: none ;
                box-shadow: none;
            }
            .inputstylo {
                background-color: #E9ECEF;
                border: none;
                border-radius: 15px;
            }

            $('#sandbox-container .input-group.date').datepicker({
                daysOfWeekHighlighted: "1,2,3,4,5",
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true
            });
        </style>
    </head>

    <body style="background-color: white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="navbar-toggler-icon"></span>
                    </button> <a class="navbar-brand" href="#">Subsecretaría</a>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="navbar-nav ml-md-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                                   data-toggle="dropdown"><?php echo $_SESSION['user']; ?></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another
                                        action</a> <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider">
                                    </div><a class="dropdown-item" href="../logout.php">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <div class="page-header p-3">
                            <br>
                            <br>
                            <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                <h6 class="mb-0 text-white lh-100">Agregar nuevo usuario:<i class="fas fa-circle"></i></h6>
                            </div>
                        </div>
                        <div class="jumbotron" style="background-color: white">
                            <form action="cbduser.php" method="POST" class="form-horizontal" onsubmit="return validar()">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                    <tr >
                                        <td>
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Usuario:</b></span>
                                                <input type="text" class="form-control inputstylo" name="usuario" id="usuario" for="usuario" placeholder="Introduce el nuevo usuario" class="form-control inputstylo">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Contraseña:</b></span>
                                                <input type="password" class="form-control inputstylo" name="pass1" id="pass1" for="pass1" placeholder="Contraseña" class="form-control inputstylo">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Confirmar contraseña:</b></span>
                                                <input type="password" class="form-control inputstylo" name="pass2" id="pass2" for="pass2" placeholder="Repite la contraseña" class="form-control inputstylo">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Nivel:</b></span>
                                                <form>
                                                    <select name="nivel" id="nivel" class="custom-select inputstylo">
                                                        <option selected>Seleccione el nivel:</option>
                                                        <option class="bg-success" value="1">Administrador</option>
                                                        <option class="bg-warning" value="2">Usuario</option>
                                                    </select>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <div class="form-group border p-2" style="background-color: #E9ECEF;border-radius: 15px;">
                                                <span><b>Responsable(Opcional):</b></span>
                                                <input type="text" class="form-control inputstylo" name="responsable" id="responsable" placeholder="Ingrese la persona responsable de subir los eventos">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-check-circle"></i>Guardar</button>
                                            <a class="btn btn-outline-danger btn-block" href="usermod.php"><i class="fas fa-times" role="button"></i> Regresar </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>

        </div>
    </div>

    </body>

    </html>
    <?php
}else{
    echo '<script> window.location="../index.php"; </script>';
}
?>