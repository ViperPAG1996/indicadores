<?php
session_start();
include '../conexion/config.php';
include '../conexion/dbconfig.php';

if(isset($_GET['delete_id']))
{
    $stmt_delete = $DB_con->prepare('DELETE FROM tbl_usuarios WHERE id =:id');
    $stmt_delete->bindParam(':id',$_GET['delete_id']);
    $stmt_delete->execute();

    header("Location: usermod.php");
}

if(isset($_SESSION['user'])) {
$usuario=$_SESSION['user'];
    $log =$conexion-> query("SELECT * FROM tbl_usuarios WHERE user='$usuario'");
    if (mysqli_num_rows($log)>0) {
        $row = mysqli_fetch_array($log);
        $nivel= $row['nivel'];
        if($nivel==1){?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta name="author" content="Pedro Aguilar Guerrero ITZ-ISC 2018">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
        <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
        <link rel="icon" href="../imagenes/ico.png">
        <title>Manejo de usuarios</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <script src="../bootstrap/js/jquery.min.js"></script>
        <script src="../bootstrap/js/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
              crossorigin="" />
        <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
                crossorigin=""></script>
                <link rel="stylesheet" type="text/css" href="../DataTables/datatables.min.css"/>

 <script type="text/javascript" src="../DataTables/datatables.min.js"></script>

 <script type="text/javascript">
    $(document).ready(function() {
        $('#lookup').DataTable( {
            "language": {
                "sProcessing":     "Procesando...",
	"sLengthMenu":     "Mostrar _MENU_ registros",
	"sZeroRecords":    "No se encontraron resultados",
	"sEmptyTable":     "Ningún dato disponible en esta tabla",
	"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	"sInfoPostFix":    "",
	"sSearch":         "Buscar:",
	"sUrl":            "",
	"sInfoThousands":  ",",
	"sLoadingRecords": "Cargando...",
	"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
	},
	"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
	}
            }
        } );

    } );

</script>
        <script type="text/javascript">
            function validar(){
                var txtAccion = document.getElementById('accion').value;
                var txtFactor = document.getElementById('factor').value;
                var txtDependencias = document.getElementById('dependencias').value;
                var txtFecha = document.getElementById('fecha_inicio').value;
                if(txtAccion == null || txtAccion.length == 0 || /^\s+$/.test(txtAccion)){
                    alert('ERROR: El campo accion no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if(txtFactor == null || txtFactor.length == 0 || /^\s+$/.test(txtFactor)){
                    alert('ERROR: El campo factor no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if(txtDependencias == null || txtDependencias.length == 0 || /^\s+$/.test(txtDependencias)){
                    alert('ERROR: El campo dep no debe ir vacío o lleno de solamente espacios en blanco');
                    return false;
                }
                if(!isNaN(txtFecha)){
                    alert('ERROR: Debe elegir una fecha');
                    return false;
                }
                return true;
            }
        </script>
        <style>
            .inputstylo:focus {
                background-color: #E9ECEF;
                border: none;
                border-radius: 15px;
                outline: none ;
                box-shadow: none;
            }
            .inputstylo {
                background-color: #E9ECEF;
                border: none;
                border-radius: 15px;
            }

            $('#sandbox-container .input-group.date').datepicker({
                daysOfWeekHighlighted: "1,2,3,4,5",
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true
            });
        </style>
    </head>
    <body style="background-color: #E9ECEF">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="navbar-toggler-icon"></span>
                    </button> <a class="navbar-brand" href="#">Subsecretaría</a>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="navbar-nav ml-md-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                                   data-toggle="dropdown"><?php echo $_SESSION['user']; ?></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="../view.php">Home</a>
                                    <a class="dropdown-item" href="../calendario/calendario.php">Ver Calendario</a>
                                    <a class="dropdown-item" href="../graficas/graficas.php">Ver Graficas</a>
                                    <div class="dropdown-divider">
                                    </div>
                                    <a class="dropdown-item" href="../logout.php">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <div class="page-header p-3">
                            <br><br>
                            <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                <h6 class="mb-0 text-white lh-100">Administración de los usuarios</h6>
                            </div>
                        </div>
                        <div class="jumbotron" style="background-color: white">
                            <a href="adduser.php" role="button"  title="Agregar Usuario" class="btn btn-outline-success"> <i class="fas fa-address-book"></i> Agregar Usuario</a>
                            <a class="btn btn-outline-danger" href="../view.php"><i class="fas fa-times" role="button"></i> Regresar </a>
                            <form method="POST" class="form-horizontal" onsubmit="return validar()">
                                <div class="table-responsive">

                              <table id="lookup" class="table table-bordered table-sm">
                              <thead bgcolor="#eeeeee" align="center">
                                        <tr>
                                        <th>Usuario/Dependencia</th>
	                                    <th>Contraseña</th>
	                                    <th>Nivel</th>
	                                    <th>Responsable</th>
	                                    <th >Opciones</th>
                                       </tr>
                                      </thead>
                                  <tbody>
                                  <?php
	$stmt = $DB_con->prepare('SELECT * FROM tbl_usuarios ORDER BY id desc');
	$stmt->execute();
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>
			<center>
			<tr>
            <!-- <td><?php echo $row['id']; ?></td> -->
						<td><?php echo $row['user']; ?></td>
						<td><?php echo $row['pw']; ?></td>
						<td><?php echo $row['nivel']; ?></td>
                        <td><?php echo $row['responsable']; ?></td>

                <td style="text-align:center;">
                    <span>
                    <div class="btn-group ">

                    <a href="moduser.php?edit_id=<?php echo $row['id']; ?>" role="button"  title="Editar datos" class="btn btn-outline-info"> <i class="fas fa-pencil-alt"></i> </a>
	                <a href="?delete_id=<?php echo $row['id']; ?>" role="button"  title="Eliminar" class="btn btn-outline-danger"> <i class="far fa-trash-alt"></i> </a>
                    </div>
                                                        </span>
                </td>
					</tr>
					</center>
			<?php
		}
	}
	else
	{
		?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; Datos no encontrados ...
            </div>
        </div>
        <?php
	}

?>
                            </tbody>
                            </table>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

    </html>
     <?php
        }
        if($nivel==1) {
            echo '<script> window.location="addnew.php"; </script>';
        }
    }
}else{
    echo '<script> window.location="../index.php"; </script>';
}
?>