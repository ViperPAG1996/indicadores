<?php
session_start();
include '../conexion/config.php';

if(isset($_SESSION['user'])) {
    $usuario=$_SESSION['user'];
    $log =$conexion-> query("SELECT * FROM tbl_usuarios WHERE user='$usuario'");
    if (mysqli_num_rows($log)>0) {
        $row = mysqli_fetch_array($log);
        $nivel= $row['nivel'];
        if($nivel==1){?>

            <!DOCTYPE html>
            <html lang="es">

            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
                <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
                <link rel="icon" href="../imagenes/ico.png">

                <title>Graficas</title>

                <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
                <script src="../bootstrap/js/jquery.min.js"></script>
                <script src="../bootstrap/js/popper.min.js"></script>
                <script src="../bootstrap/js/bootstrap.min.js"></script>

                <link href="../bootstrap/css/fontawesome-all.css" rel="stylesheet">
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
                      crossorigin="" />
                <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
                        crossorigin=""></script>

                <style>
                </style>
                <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

                <script type="text/javascript" src="DataTables/datatables.min.js"></script>

                <script>
                    $(document).ready(function() {
                        var dataTable = $('#lookup').DataTable( {

                            "language":	{
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Último",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },

                            "processing": true,
                            "serverSide": true,
                            "ajax":{
                                url :"ajax-grid-data.php", // json datasource
                                type: "post",  // method  , by default get
                                error: function(){  // error handling
                                    $(".lookup-error").html("");
                                    $("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                                    $("#lookup_processing").css("display","none");

                                }
                            }
                        });
                        dataTable.columns( [ 0 ] ).visible( false, false );
                    });
                </script>
                <script src="../librerias/jquery-3.3.1.min.js"></script>
                <script src="../librerias/plotly-latest.min.js"></script>
            </head>

            <body style="background-color: #E9ECEF">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg bg-dark navbar-dark fixed-top">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="navbar-toggler-icon"></span>
                            </button> <a class="navbar-brand" href="#">Subsecretaría </a>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="navbar-nav ml-md-auto">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                                           data-toggle="dropdown"><?php echo $_SESSION['user']; ?></a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item" href="../view.php">Home</a>
                                            <a class="dropdown-item" href="../calendario/calendario.php">Ver Calendario</a>
                                            <a class="dropdown-item" href="../usuarios/usermod.php">Manejo de usuarios</a>
                                            <div class="dropdown-divider">
                                            </div>
                                            <a class="dropdown-item" href="../logout.php">Cerrar Sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="row">
                            <main role="main" class="container">

                                <div class="page-header p-3">
                                    <br>
                                    <br>
                                    <div class="d-flex p-3 my-3 text-white-50 bg-dark rounded shadow-sm justify-content-center">
                                        <h6 class="mb-0 text-white lh-100">Graficacion de eventos <i class="fas fa-circle"></i></h6>
                                    </div>
                                </div>
                                <div class="p-3 bg-white rounded shadow-sm">
                                    <ul class="nav nav-tabs nav-justified">
                                        <!--<li class="nav-item">
                                            <a class="nav-link active" href="#">Vista General</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="../reportes.php">Reportes</a>
                                        </li>-->
                                    </ul>
                                    <br>
                                    <!-- Button to Open the Modal -->
                                    <a class="btn btn-outline-danger" href="../view.php"><i class="fas fa-times" role="button"></i> Regresar </a>
                                    <!-- The Modal -->
                                    <div class="modal fade" id="myModal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Seleccione tipo de evento:</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body" style="">
                                                    <a href="addnewrep.php" class="btn btn-dark" role="button">Actividad Repetitiva</a>
                                                    <a href="../actividades/addnewinf.php" class="btn btn-dark" role="button">Infraestructura</a>
                                                    <a href="addnew.php" class="btn btn-dark" role="button">Actividad</a>
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="media text-muted pt-3">
                                    </div>

                                    <small class="d-block text-right mt-3">

                                    </small>
                                    <div>
                                        <?php
                                        require_once 'tabla.php';
                                        $conexion=conexion();
                                        $sql= "SELECT title,inicio_normal FROM tbl_actividades";
                                        $result= mysqli_query($conexion,$sql);
                                        $valoresX=array();
                                        $valoresY=array();

                                        while ($ver=mysqli_fetch_row($result)) {
                                            $valoresX[]=$ver[0];
                                            $valoresY[]=$ver[1];
                                        }
                                        $datosX= json_encode($valoresX);
                                        $datosY= json_encode($valoresY);

                                        ?>
                                        <!-- start of user-activity-graph -->
                                        <div id="graficaBarras" style="width:100%; height:100%;"></div>
                                    </div>
                                </div>
                            </main>
                        </div>

                    </div>
                </div>
            </div>
            </div>

            </body>
            <script type="text/javascript">
                function crearCadenaLineal(json) {
                    var parsed = JSON.parse(json);
                    var arr = [];
                    for (var x in parsed) {
                        arr.push(parsed[x]);
                    }
                    return arr;
                }
            </script>
            <script type="text/javascript">
                datosX = crearCadenaLineal('<?php  echo $datosX ?>');
                datosY = crearCadenaLineal('<?php  echo $datosY ?>');
                var data = [{
                    x: datosX,
                    y: datosY,
                    type: 'bar'
                }];

                Plotly.newPlot('graficaBarras', data);
            </script>

            </html>
            <?php
        }
        if($nivel==2) {
            echo '<script> window.location="addnew.php"; </script>';
        }
    }
}else{
    echo '<script> window.location="../index.php"; </script>';
}
?>