<?php
	session_start();
	include './conexion/config.php';
	if(isset($_SESSION['user'])){
	echo '<script> window.location="index.php"; </script>';
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Portal  web informativo  de la Subsecretaría de Prevención Social del Delito ">
    <meta name="author" content="José Miguel Flores Romo ITZ-ISC 2018">
    <link rel="icon" href="../imagenes/ico.png">

    <title>LOGIN</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href="bootstrap/css/fontawesome-all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
            crossorigin=""></script>
</head>
<body>
<div class="container">
<div class="col-12"></div>
<div class="col-12">
            <br><br>
            <center>
                <h1><img src="sub.jpg" style="width: 70%; height: 90%"></h1>
                <h1 class="text-black-50 text-center">Herramienta de Indicadores Delictivos del Estado de Zacatecas</h1>
			<h1 class="h1 text-center" style="color:black">Login</h1>
			<form method="post" action="usuarios/validar.php" class="align-content-center">
				<input type="text" placeholder="usuario" class="form-control text-center" name="user" autocomplete="off" required><br><br>
				<input type="password" placeholder="contraseña" class="form-control text-center" name="pw" autocomplete="off" required><br><br>
                <input type="submit" class="btn btn-success text-center" name="login" value="Entrar">
			</form>
            </center>
</div>
<div class="col-12"></div>
</div>
</body>
</html>